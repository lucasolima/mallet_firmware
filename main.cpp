/*
 * main.cpp
 *
 *  Created on: May 13, 2016
 *      Author: lucasolim
 */

#include <iostream>
#include <fstream>
#include "inc/mallet_threads.hpp"
#include "inc/cConfigurations.hpp"
#include <sched.h>
#include "inc/modem.h"

#include <signal.h>



#define BD_PATH "/flash_partition/main_db.json"

using namespace std;

struct sched_param thread_param;


int main()
{
	signal(SIGINT,sigint_handle);

	thread_param.__sched_priority = 0;

	fprintf(stderr, "Min: %d\n", sched_get_priority_min(SCHED_OTHER));

	fprintf(stderr, "Max: %d\n", sched_get_priority_max(SCHED_OTHER));

	pthread_setschedparam(thread_keyscreen,SCHED_OTHER,&thread_param);

	cConfigurations my_config(BD_PATH);


	pthread_create(&thread_keyscreen,NULL,&Thread_KeyScreen,&my_config);


	while(1)
	{
		sleep(10000);

		if (FlagSigint == 1)
		{
			flag_keyboard = 0;
			perror("kill1\n");
			pthread_join(thread_keyscreen,NULL);
			perror("kill2\n");
			return 0;
		}

	}
	perror("kill3\n");
	return 0;
}



