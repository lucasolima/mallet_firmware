/*
 * cConfigurations.cpp
 *
 *  Created on: May 13, 2016
 *      Author: lucasolim
 */

#include "inc/cConfigurations.hpp"

cConfigurations::cConfigurations(string ConfigurationFile) : BD_Path(ConfigurationFile)//, Blue(RadConfig)
{
	ifstream InFile(BD_Path.c_str(), ios::binary);

	if(!InFile.good())
	{

		perror("Opening New Database\n");
		ofstream OutFile(BD_Path.c_str());

		Root["Bluetooth"]["StateOnOff"] = Enable;
		Root["Bluetooth"]["Pin"]        = "123456";
		Root["Bluetooth"]["Name"] 		= "TRC-1193";
		Root["Bluetooth"]["Ip"]			= "192.168.0.17";
		Root["Bluetooth"]["Netmask"]    = "255.255.255.0";

		Root["Ethernet"]["Ip"] 			= "192.168.0.2";
		Root["Ethernet"]["Netmask"]		= "255.255.255.0";

		Root["Audio"]["Volume"]["Speaker"] = GetIntHwParam(AudioSpkVol);
		Root["Audio"]["Volume"]["Mic"] 	   = GetIntHwParam(AudioMicVol);

		Root["ActiveChannel"]= 0;


		Root["Tdm"]["TimeSlot0"] = 0;
		Root["Tdm"]["TimeSlot1"] = 1;
		Root["Tdm"]["TimeSlot2"] = 2;
		Root["Tdm"]["TimeSlot3"] = 3;

		Root["Settings"]["TimeDivisionMultiplex"] = Disable;
		Root["Settings"]["IpForwardingSameFrequency"] = Disable;
		Root["Settings"]["IpForwardingDiffFrequency"] = Disable; //Experimental
		Root["Settings"]["LcdBacklightTimer"] = Enable;
		Root["Settings"]["IsVeicular"] = GetIntHwParam(IsVeicular);
		Root["Settings"]["PowerAmplifier"] = High; //Low, Medium Low, High, Medium High
		Root["Settings"]["BatteryLevel"] = GetIntHwParam(BatteryLevel);
		Root["Settings"]["Rssi"] = GetIntHwParam(Rssi);

		Root["GPS"]["StateOnOff"] = Enable;
		Root["GPS"]["Mode"] = Raw;

		Root["PTT"]["0"] = Disable;
		Root["PTT"]["1"] = Disable;
		Root["PTT"]["2"] = Disable;
		Root["PTT"]["3"] = Disable;

		Root["IfacesUp"];


//		Root["TextMessage"]["Outcome"]["Msg1"]["Text"] = "";
//		Root["TextMessage"]["Outcome"]["Msg1"]["Timestamp"] = "";
//		Root["TextMessage"]["Outcome"]["Msg1"]["Channel"] = "";
//
//		Root["TextMessage"]["Income"]["Msg1"]["Text"] = "";
//		Root["TextMessage"]["Income"]["Msg1"]["Timestamp"] = "";
//		Root["TextMessage"]["Income"]["Msg1"]["Channel"] = "";

		Root["Channels"][0]["Name"]	   		 = "Rede0";
		Root["Channels"][0]["FqTx"]	   		 = 30000;
		Root["Channels"][0]["FqRx"]	  		 = 30000;
		Root["Channels"][0]["Vocoder"]		 = TWELP;
		Root["Channels"][0]["Modulation"]	 = FSK4;
		Root["Channels"][0]["AudioType"]	 = "Digital";
		Root["Channels"][0]["Ip"]	 		 = "192.168.1.1";
		Root["Channels"][0]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][0]["Crypto"]	     = None;
		Root["Channels"][0]["Key"]			 = 0;

		Root["Channels"][1]["Name"]	   		 = "Rede1";
		Root["Channels"][1]["FqTx"]	   		 = 35000;
		Root["Channels"][1]["FqRx"]	  		 = 35000;
		Root["Channels"][1]["Vocoder"]		 = TWELP;
		Root["Channels"][1]["Modulation"]	 = FSK4;
		Root["Channels"][1]["AudioType"]	 = "Digital";
		Root["Channels"][1]["Ip"]	 		 = "192.168.2.1";
		Root["Channels"][1]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][1]["Crypto"]	     = AES256;
		Root["Channels"][1]["Key"]			 = 0;

		Root["Channels"][2]["Name"]	   		 = "Rede2";
		Root["Channels"][2]["FqTx"]	   		 = 40000;
		Root["Channels"][2]["FqRx"]	  		 = 40000;
		Root["Channels"][2]["Vocoder"]		 = TWELP;
		Root["Channels"][2]["Modulation"]	 = FSK4;
		Root["Channels"][2]["AudioType"]	 = "Analog";
		Root["Channels"][2]["Ip"]	 		 = "192.168.3.1";
		Root["Channels"][2]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][2]["Crypto"]	     = AES128;
		Root["Channels"][2]["Key"]			 = 0;

		Root["Channels"][3]["Name"]	   		 = "Rede3";
		Root["Channels"][3]["FqTx"]	   		 = 45000;
		Root["Channels"][3]["FqRx"]	  		 = 45000;
		Root["Channels"][3]["Vocoder"]		 = TWELP;
		Root["Channels"][3]["Modulation"]	 = FSK4;
		Root["Channels"][3]["AudioType"]	 = "Digital";
		Root["Channels"][3]["Ip"]	 		 = "192.168.4.1";
		Root["Channels"][3]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][3]["Crypto"]	     = AES256;
		Root["Channels"][3]["Key"]			 = 0;

		Root["Channels"][4]["Name"]	   		 = "Rede4";
		Root["Channels"][4]["FqTx"]	   		 = 50000;
		Root["Channels"][4]["FqRx"]	  		 = 50000;
		Root["Channels"][4]["Vocoder"]		 = TWELP;
		Root["Channels"][4]["Modulation"]	 = FSK4;
		Root["Channels"][4]["AudioType"]	 = "Digital";
		Root["Channels"][4]["Ip"]	 		 = "192.168.5.1";
		Root["Channels"][4]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][4]["Crypto"]	     = AES64;
		Root["Channels"][4]["Key"]			 = 0;

		Root["Channels"][5]["Name"]	   		 = "Rede5";
		Root["Channels"][5]["FqTx"]	   		 = 55000;
		Root["Channels"][5]["FqRx"]	  		 = 55000;
		Root["Channels"][5]["Vocoder"]		 = TWELP;
		Root["Channels"][5]["Modulation"]	 = FSK4;
		Root["Channels"][5]["AudioType"]	 = "Digital";
		Root["Channels"][5]["Ip"]	 		 = "192.168.6.1";
		Root["Channels"][5]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][5]["Crypto"]	     = AES192;
		Root["Channels"][5]["Key"]			 = 0;

		Root["Channels"][6]["Name"]	   		 = "Rede6";
		Root["Channels"][6]["FqTx"]	   		 = 60000;
		Root["Channels"][6]["FqRx"]	  		 = 60000;
		Root["Channels"][6]["Vocoder"]		 = TWELP;
		Root["Channels"][6]["Modulation"]	 = FSK4;
		Root["Channels"][6]["AudioType"]	 = "Digital";
		Root["Channels"][6]["Ip"]	 		 = "192.168.7.1";
		Root["Channels"][6]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][6]["Crypto"]	     = AES256;
		Root["Channels"][6]["Key"]			 = 0;

		Root["Channels"][7]["Name"]	   		 = "Rede7";
		Root["Channels"][7]["FqTx"]	   		 = 65000;
		Root["Channels"][7]["FqRx"]	  		 = 65000;
		Root["Channels"][7]["Vocoder"]		 = TWELP;
		Root["Channels"][7]["Modulation"]	 = FSK4;
		Root["Channels"][7]["AudioType"]	 = "Digital";
		Root["Channels"][7]["Ip"]	 		 = "192.168.8.1";
		Root["Channels"][7]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][7]["Crypto"]	     = None;
		Root["Channels"][7]["Key"]			 = 0;

		Root["Channels"][8]["Name"]	   		 = "Rede8";
		Root["Channels"][8]["FqTx"]	   		 = 70000;
		Root["Channels"][8]["FqRx"]	  		 = 70000;
		Root["Channels"][8]["Vocoder"]		 = TWELP;
		Root["Channels"][8]["Modulation"]	 = FSK4;
		Root["Channels"][8]["AudioType"]	 = "Digital";
		Root["Channels"][8]["Ip"]	 		 = "192.168.9.1";
		Root["Channels"][8]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][8]["Crypto"]	     = None;
		Root["Channels"][8]["Key"]			 = 0;

		Root["Channels"][9]["Name"]	   		 = "Rede9";
		Root["Channels"][9]["FqTx"]	   	 	 = 75000;
		Root["Channels"][9]["FqRx"]	  	 	 = 75000;
		Root["Channels"][9]["Vocoder"]		 = TWELP;
		Root["Channels"][9]["Modulation"]	 = FSK4;
		Root["Channels"][9]["AudioType"]	 = "Digital";
		Root["Channels"][9]["Ip"]	 		 = "192.168.10.1";
		Root["Channels"][9]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][9]["Crypto"]	     = None;
		Root["Channels"][9]["Key"]			 = 0;

		Root["Channels"][10]["Name"]	   	 = "Rede10";
		Root["Channels"][10]["FqTx"]	   	 = 80000;
		Root["Channels"][10]["FqRx"]	  	 = 80000;
		Root["Channels"][10]["Vocoder"]		 = TWELP;
		Root["Channels"][10]["Modulation"]	 = FSK4;
		Root["Channels"][10]["AudioType"]	 = "Digital";
		Root["Channels"][10]["Ip"]	 		 = "192.168.11.1";
		Root["Channels"][10]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][10]["Crypto"]	     = None;
		Root["Channels"][10]["Key"]			 = 0;

		Root["Channels"][11]["Name"]	   	 = "Rede11";
		Root["Channels"][11]["FqTx"]	   	 = 85000;
		Root["Channels"][11]["FqRx"]	  	 = 85000;
		Root["Channels"][11]["Vocoder"]		 = TWELP;
		Root["Channels"][11]["Modulation"]	 = FSK4;
		Root["Channels"][11]["AudioType"]	 = "Digital";
		Root["Channels"][11]["Ip"]	 		 = "192.168.12.1";
		Root["Channels"][11]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][11]["Crypto"]	     = None;
		Root["Channels"][11]["Key"]			 = 0;

		Root["Channels"][12]["Name"]	   	 = "Rede12";
		Root["Channels"][12]["FqTx"]	   	 = 30200;
		Root["Channels"][12]["FqRx"]	  	 = 30200;
		Root["Channels"][12]["Vocoder"]		 = TWELP;
		Root["Channels"][12]["Modulation"]	 = FSK4;
		Root["Channels"][12]["AudioType"]	 = "Digital";
		Root["Channels"][12]["Ip"]	 		 = "192.168.13.1";
		Root["Channels"][12]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][12]["Crypto"]	     = None;
		Root["Channels"][12]["Key"]			 = 0;

		Root["Channels"][13]["Name"]	   	 = "Rede13";
		Root["Channels"][13]["FqTx"]	   	 = 87025;
		Root["Channels"][13]["FqRx"]	  	 = 87025;
		Root["Channels"][13]["Vocoder"]		 = TWELP;
		Root["Channels"][13]["Modulation"]	 = FSK4;
		Root["Channels"][13]["AudioType"]	 = "Digital";
		Root["Channels"][13]["Ip"]	 		 = "192.168.14.1";
		Root["Channels"][13]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][13]["Crypto"]	     = None;
		Root["Channels"][13]["Key"]			 = 0;

		Root["Channels"][14]["Name"]	   	 = "Rede14";
		Root["Channels"][14]["FqTx"]	   	 = 30000;
		Root["Channels"][14]["FqRx"]	  	 = 40000;
		Root["Channels"][14]["Vocoder"]		 = TWELP;
		Root["Channels"][14]["Modulation"]	 = FSK4;
		Root["Channels"][14]["AudioType"]	 = "Digital";
		Root["Channels"][14]["Ip"]	 		 = "192.168.15.1";
		Root["Channels"][14]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][14]["Crypto"]	     = None;
		Root["Channels"][14]["Key"]			 = 0;

		Root["Channels"][15]["Name"]	   	 = "Rede15";
		Root["Channels"][15]["FqTx"]	   	 = 40000;
		Root["Channels"][15]["FqRx"]	  	 = 30000;
		Root["Channels"][15]["Vocoder"]		 = TWELP;
		Root["Channels"][15]["Modulation"]	 = FSK4;
		Root["Channels"][15]["AudioType"]	 = "Digital";
		Root["Channels"][15]["Ip"]	 		 = "192.168.16.1";
		Root["Channels"][15]["Netmask"]	     = "255.255.255.0";
		Root["Channels"][15]["Crypto"]	     = None;
		Root["Channels"][15]["Key"]			 = 0;

		OutFile << Root;
		OutFile.close();

	}
	else
	{
		InFile >> Root;
		InFile.close();
	}
	//UpdateTdmChannel();
	UpdateChannel();
	UpdateBluetooth();
	UpdateEthernet();
	PttOff();


	//InitData();
}
cConfigurations::~cConfigurations()
{

}




int cConfigurations::UpdateFile()
{
	ofstream OutFile(BD_Path.c_str(), ios::binary);
	OutFile << Root ;
	OutFile.close();
}

int cConfigurations::UpdateChannel()
{
	u8 buffer[4];
	int freq;
	int fd;
	int error;
	int i;
	int channel = Root["ActiveChannel"].asInt();
	string cmd;

	if(channel >= Root["Channels"].size())
	{
		fprintf(stderr, "Invalid Channel: %d\n", channel );
		return -1;
	}

	fd = open("/dev/RxTx_Controller",O_WRONLY);
	if(fd < 0)
		fprintf(stderr, "Erro de acesso ao dispositivo\n");

	if(Root["Channels"][channel]["AudioType"].asString() == "Digital")
	{
		buffer[0] = CHANGE_MODCOD;
		buffer[1] = (Root["Channels"][channel]["Modulation"].asInt() & 0xFF00) >> 8;
		buffer[2] = Root["Channels"][channel]["Modulation"].asInt() & 0x00FF;
		buffer[3] = Root["Channels"][channel]["Vocoder"].asInt();
		error = write(fd,buffer,4);
		if(error < 0)
			fprintf(stderr, "Erro de acesso ao dispositivo\n");
	}
	else if(Root["Channels"][channel]["AudioType"].asString() == "Analog")
	{
		buffer[0] = CHANGE_MODCOD;
		buffer[1] = (FM & 0xFF00) >> 8;
		buffer[2] = FM & 0x00FF;
		buffer[3] = TWELP;
		error = write(fd,buffer,4);
		if(error < 0)
			fprintf(stderr, "Erro de acesso ao dispositivo\n");
	}



	freq = Root["Channels"][channel]["FqRx"].asInt();
	freq = ((freq - 30000) * 4) / 25;
	buffer[0] = MODEM_SET_FREQ_RX;
	buffer[1] = (freq & 0xFF00) >> 8;
	buffer[2] = freq & 0x00FF;
	buffer[3] = 0;
	error = write(fd,buffer,4);
	if(error < 0)
		fprintf(stderr, "Erro de acesso ao dispositivo\n");

	freq = Root["Channels"][channel]["FqTx"].asInt();
	freq = ((freq - 30000) * 4) / 25;
	buffer[0] = MODEM_SET_FREQ_TX;
	buffer[1] = (freq & 0xFF00) >> 8;
	buffer[2] = freq & 0x00FF;
	buffer[3] = 0;
	error = write(fd,buffer,4);
	if(error < 0)
		fprintf(stderr, "Erro de acesso ao dispositivo\n");


	char buf[8];

	while(Root["IfacesUp"].size() > 0)
	{
		Json::Value *temp;
		temp = new Json::Value;
		Root["IfacesUp"].removeIndex(Root["IfacesUp"].size() - 1, temp);
		string cmd_tmp = "ifconfig " + temp->asString() + " down";
		delete temp;
		system(cmd_tmp.c_str());
	}

	sprintf(buf, "eth%d  ", channel + 1);
	string buf2;
	buf2.assign(buf,6);

	Root["IfacesUp"].append(buf2.c_str());

	cmd = "ifconfig " + buf2 + Root["Channels"][channel]["Ip"].asString() + " netmask " + Root["Channels"][channel]["Netmask"].asString() + " up > /var/log/dump";
	//cout << cmd << endl;
	system(cmd.c_str());
	close(fd);
	return channel;


}

int cConfigurations::UpdateTdmChannel()
{
	u8 buffer[4];
	int freq;
	int fd;
	int error;
	string cmd;
	int z = 0;
	Json::ValueIterator j;

	for(j = Root["Tdm"].begin() ; j != Root["Tdm"].end() ; j++)
	{
		z++;

		int channel = j->asInt();

		if(channel >= Root["Channels"].size())
		{
			fprintf(stderr, "Invalid Channel: %d\n", channel );
			return -1;
		}

		fd = open("/dev/RxTx_Controller",O_WRONLY);
		if(fd < 0)
			fprintf(stderr, "Erro de acesso ao dispositivo\n");

		if(Root["Channels"][channel]["AudioType"].asString() == "Digital")
		{
			buffer[0] = CHANGE_MODCOD;
			buffer[1] = (Root["Channels"][channel]["Modulation"].asInt() & 0xFF00) >> 8;
			buffer[2] = Root["Channels"][channel]["Modulation"].asInt() & 0x00FF;
			buffer[3] = Root["Channels"][channel]["Vocoder"].asInt();
			error = write(fd,buffer,4);
			if(error < 0)
				fprintf(stderr, "Erro de acesso ao dispositivo\n");
		}
		else if(Root["Channels"][channel]["AudioType"].asString() == "Analog")
		{
			buffer[0] = CHANGE_MODCOD;
			buffer[1] = (FM & 0xFF00) >> 8;
			buffer[2] = FM & 0x00FF;
			buffer[3] = TWELP;
			error = write(fd,buffer,4);
			if(error < 0)
				fprintf(stderr, "Erro de acesso ao dispositivo\n");
		}

		freq = Root["Channels"][channel]["FqRx"].asInt();
		freq = ((freq - 30000) * 4) / 25;
		buffer[0] = MODEM_SET_FREQ_RX;
		buffer[1] = z;
		buffer[2] = (freq & 0xFF00) >> 8;
		buffer[3] = freq & 0x00FF;
		error = write(fd,buffer,4);
		if(error < 0)
			fprintf(stderr, "Erro de acesso ao dispositivo\n");

		freq = Root["Channels"][channel]["FqTx"].asInt();
		freq = ((freq - 30000) * 4) / 25;
		buffer[0] = MODEM_SET_FREQ_TX;
		buffer[1] = z;
		buffer[2] = (freq & 0xFF00) >> 8;
		buffer[3] = freq & 0x00FF;
		error = write(fd,buffer,4);
		if(error < 0)
			fprintf(stderr, "Erro de acesso ao dispositivo\n");

		char buf[8];

		sprintf(buf, "eth%d  ", channel);
		string buf2;
		buf2.assign(buf,6);

		cmd = "ifconfig " + buf2 + Root["Channels"][channel]["Ip"].asString() + " netmask " + Root["Channels"][channel]["Netmask"].asString() + " up > /var/log/dump";
		cout << cmd << endl;
		system(cmd.c_str());
		close(fd);
		break;
	}

	return 0;
}

int cConfigurations::UpdateBluetooth()
{
	string cmd;
	Blue.mChangeName(Root["Bluetooth"]["Name"].asCString());
	Blue.mChangePin(Root["Bluetooth"]["Pin"].asCString());
	if(Root["Bluetooth"]["StateOnOff"].asInt() == Disable) Blue.mTurnOff();
	else Blue.mTurnOn();
	char buf_temp[100];
	cmd = "ifconfig pan0 " + Root["Bluetooth"]["Ip"].asString() + " netmask " + Root["Bluetooth"]["Netmask"].asString() + " > /var/log/dump";
	system(cmd.c_str());
	return 0;

}

int cConfigurations::UpdateEthernet()
{
	string cmd;
	cmd = "ifconfig eth0 " + Root["Ethernet"]["Ip"].asString() + " netmask " + Root["Ethernet"]["Netmask"].asString() + " > /var/log/dump";
	system(cmd.c_str());
	return 0;
}

void cConfigurations::PttOn()
{
	int fd = open("/dev/RxTx_Controller", O_WRONLY);
	char buf[2];
	buf[0] = PTT_ON;
	buf[1] = 0;
	write(fd,buf,2);
	close(fd);
	Root["PTT"]["0"] = Enable;
}

void cConfigurations::PttOff()
{
	int fd = open("/dev/RxTx_Controller", O_WRONLY);
	char buf[2];
	buf[0] = PTT_OFF;
	buf[1] = 0;
	write(fd,buf,2);
	close(fd);
	Root["PTT"]["0"] = Disable;
}


int cConfigurations::UpdateTdmStatus(bool status)
{
	//disable/enable TDM
	return 0;
}

int cConfigurations::GetIntHwParam(HwParam p)
{
	return 0;
}





/*
#define GPS_PORTA_UDP_1				0x0078;
#define GPS_PORTA_UDP_2				0x0079;
#define REDE_ETH_IP					0x007A;
#define REDE_ETH_MASC				0x007B;
#define REDE_ETH_IP_PC				0x007C;
#define REDE_RADIO_IP				0x007D;
#define REDE_RADIO_MASC				0x007E;
#define REDE_ROTAS_ROTA_PADRAO		0x007F;
#define REDE_ROTAS					0x0080;
#define FUNCIONAMENTO_POTENCIA		0x0081;
#define FUNCIONAMENTO_EMPREGO		0x0082;
#define DIVERSOS_VERSAO				0x0083;
#define DIVERSOS_SENHAS_POWER_ON	0x0084;
#define	DIVERSOS_SENHAS_CONFIG		0x0085;
#define DIVERSOS_HORA_REMOTA		0x0086;
#define DIVERSOS_TENSAO				0x0087;
#define DIVERSOS_SINAL_RECEBIDO		0x0088;
#define DIVERSOS_APAGA_MEMORIA		0x0089;*/
