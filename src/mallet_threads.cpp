/*
 * mallet_threads.cpp
 *
 *  Created on: May 13, 2016
 *      Author: lucasolim
 */
#include "../inc/mallet_threads.hpp"
#include "../inc/modem.h"

bool FlagSigint = 0;
void sigint_handle(int sig);



pthread_t thread_keyscreen;
void *Thread_KeyScreen(void *ptr);
bool flag_keyboard = 1;
int ret_keyboard = 0;


void sigint_handle(int sig)
{
	flag_keyboard = 0;
	FlagSigint = 1;
}

int SetVolume(int Vol)
{
	int fdtemp;
	fdtemp = open("/dev/mcp4561_0.0x2F",O_WRONLY);
	if(fdtemp < 0)
	{
		perror("Pot Spk");
		return fdtemp;;
	}
	char buf_cmd[2];
	buf_cmd[0] = 0;
	if(Vol == 1) buf_cmd[1] = 70;
	else if(Vol == 2) buf_cmd[1] = 140;
	else if(Vol == 3) buf_cmd[1] = 210;
	else if(Vol == 4) {
		buf_cmd[0] = 1;
		buf_cmd[1] = 0;
	}
	else
	{
		buf_cmd[0] = 0;
		buf_cmd[1] = 0;
	}


	write(fdtemp,buf_cmd,2);
	close(fdtemp);
	return 0;
}


void *Thread_KeyScreen(void *ptr)
{

	flag_keyboard = 1;
	int screen_cnt = 0;
	int fd;
	struct input_event my_input_buf[3];
	struct timeval timeout;
	fd_set set;
	cConfigurations& my_config = *(cConfigurations*)ptr;
	cMenu *my_menu;
	my_menu = new cMainMenu(my_config,(cMenu*)NULL);
	int menu_timeout = 0;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;//500000;

	bool flag_number = 0;
	int number = 0;

	fd = open("/dev/input/event0",O_RDONLY);

	int rv;

	while(flag_keyboard)
	{

		FD_ZERO(&set); /* clear the set */
		FD_SET(fd, &set); /* add our file descriptor to the set */

		timeout.tv_sec = 10;
		timeout.tv_usec = 0;//500000;

		rv = select(fd + 1, &set, NULL, NULL, &timeout);

		//fprintf(stderr, "Rv: %d\n", rv);



		if(rv > 0) //(rv != 0 && rv != -1)
		{
			fprintf(stderr,"loop!\n");

			menu_timeout = 0;

			if(screen_cnt >= 5 )
			{
				screen_cnt = 0;
				my_menu->mUpdate();
			}

			read(fd,my_input_buf,1 * sizeof(input_event));

			if(my_input_buf[0].value == 1  && my_input_buf[0].code == 33)
			{
				my_config.Root["Audio"]["Volume"]["Speaker"] = 0;
				fprintf(stderr,"Volume 0\n");
				SetVolume(0);

			}
			else if(my_input_buf[0].value == 1  && my_input_buf[0].code == 34)
			{
				my_config.Root["Audio"]["Volume"]["Speaker"] = 1;
				fprintf(stderr,"Volume 1\n");
				SetVolume(1);
			}
			else if(my_input_buf[0].value == 1  && my_input_buf[0].code == 35)
			{
				my_config.Root["Audio"]["Volume"]["Speaker"] = 2;
				fprintf(stderr,"Volume 2\n");
				SetVolume(2);
			}
			else if(my_input_buf[0].value == 1  && my_input_buf[0].code == 36)
			{
				my_config.Root["Audio"]["Volume"]["Speaker"] = 3;
				fprintf(stderr,"Volume 3\n");
				SetVolume(3);

			}
			else if(my_input_buf[0].value == 1  && my_input_buf[0].code == 37)
			{
				my_config.Root["Audio"]["Volume"]["Speaker"] = 4;
				fprintf(stderr,"Volume 4\n");
				SetVolume(4);
			}

			if(my_input_buf[0].value == 1  && my_input_buf[0].code == 39)
			{
				my_config.Root["PTT"]["0"] = 1;
				fprintf(stderr, "PTT 1\n");
				my_menu->mUpdate();
				my_config.PttOn();

			}
			else if(my_input_buf[0].value == 0 && my_input_buf[0].code == 39)
			{

				my_config.Root["PTT"]["0"] = 0;
				fprintf(stderr, "PTT 0\n");
				my_menu->mUpdate();
				my_config.PttOff();
			}


			if(my_input_buf[0].value == 1 && my_menu->function_is_running_flag == 1 && (my_input_buf[0].code >= 0x30 ) && (my_input_buf[0].code <= 0x3E )  )
			{
				my_menu->function_key_pressed = my_input_buf[0].code;
				my_menu->mGoToChild();
			}
			else
			{

				if(my_input_buf[0].value == 1)
				{


					if(my_input_buf[0].code >= 48 && my_input_buf[0].code <= 57)
					{
						if(flag_number == 0)
						{
							number = my_input_buf[0].code - 48;
							my_menu->mJumpToLine(number);
							flag_number = 1;
						}
						else
						{
							number = (10 * number) + my_input_buf[0].code - 48;
							my_menu->mJumpToLine(number);
							flag_number = 0;
							number = 0;
						}
					}
					else if(flag_number == 1)
					{
						flag_number = 0;
						number = 0;
					}

					if(my_input_buf[0].code == 58)
					{
						my_menu->mScrollUp(1);
					}
					else if(my_input_buf[0].code == 59)
					{
						if(my_menu->mGetParent() != NULL)
						{

							my_menu = my_menu->mGoToParent();

							my_menu->mPrintMenu();

						}
					}
					else if(my_input_buf[0].code == 60)
					{
						my_menu = my_menu->mErase();
					}
					else if(my_input_buf[0].code == 61)
					{
						my_menu->mScrollDown(1);
					}
					else if(my_input_buf[0].code == 62)
					{
						my_menu = my_menu->mGoToChild();
					}
				}
			}


		}
		else
		{

			menu_timeout++;
			screen_cnt ++;
			//perror("timeout++\n");
			if(my_menu->function_text)
			{
				my_menu->function_flag_timeout = 1;
				my_menu->mGoToChild();
				my_menu->function_flag_timeout = 0;
			}
			else if(menu_timeout > 5 && !my_menu->function_is_running_flag)
			{
				menu_timeout = 0;
				while(my_menu->mGetParent() != NULL)
					my_menu = my_menu->mGoToParent();


				if(screen_cnt >= 5 && my_config.Root["Settings"]["LcdBacklightTimer"] == 0)
				{
					cout << (char)18 << flush;
				}
				else
				{
					//my_menu->mUpdate();

				}
			}
			else if(menu_timeout > 10 && my_menu->function_is_running_flag)
			{
				my_menu->function_key_pressed = 59;
				my_menu = my_menu->mGoToChild();
				menu_timeout = 0;
				while(my_menu->mGetParent() != NULL)
					my_menu = my_menu->mGoToParent();
				//my_menu->mUpdate();

			}
		}
		//close(fd);
		//if(screen_cnt < 5 || my_config.RadConfig.Luz_Lcd_Tempo == 1) my_menu->mUpdate();
	}



	close(fd);

}
