/*
 * cMenu9.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu9.hpp"

cMenu9::cMenu9(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 9;
	menu_art = new string[length + 1];

	menu_art[0] = "__9. Configuracoes__";
	menu_art[1] = "1. Modem            ";
	menu_art[2] = "2. Audio            ";
	menu_art[3] = "3. Salto em Freq    ";
	menu_art[4] = "4. Acesso ao meio   ";
	menu_art[5] = "5. GPS              ";
	menu_art[6] = "6. Monitoramento    ";
	menu_art[7] = "7. Rede             ";
	menu_art[8] = "8. Funcionamento    ";
	menu_art[9] = "9. Diversos         ";

	mPrintMenu();

}

cMenu9::~cMenu9() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu9::mGoToChild()
{
	if(actual_line == 1) return new cMenu91(config,this);
	if(actual_line == 2) return new cMenu92(config,this);
	if(actual_line == 3) return new cMenu93(config,this);
	if(actual_line == 4) return new cMenu94(config,this);
	if(actual_line == 5) return new cMenu95(config,this);
	if(actual_line == 6) return new cMenu96(config,this);
	if(actual_line == 7) return new cMenu97(config,this);
	if(actual_line == 8) return new cMenu98(config,this);
	if(actual_line == 9) return new cMenu99(config,this);

	return NULL;
}

