/*
 * cMenu6.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu6.hpp"

cMenu6::cMenu6(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;
	menu_art = new string[length + 1];
	menu_art[0] = "__6. Luz do Visor___";
	menu_art[1] = "1. Ligada           ";
	menu_art[2] = "2. 5 segundos       ";

	mPrintMenu();

}



cMenu6::~cMenu6() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu6::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;
}

cMenu * cMenu6::Function1()
{
	//implementar aqui
	config.Root["Settings"]["LcdBacklightTimer"] = 1;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo   ");
	mPrintMenu();
	return this;
}

cMenu * cMenu6::Function2()
{
	//implementar aqui
	config.Root["Settings"]["LcdBacklightTimer"]  = 0;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo   ");
	mPrintMenu();
	return this;
}


