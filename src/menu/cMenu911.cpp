/*
 * cMenu911.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu911.hpp"


cMenu911::cMenu911(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 3;

	menu_art = new string[length+1];

	menu_art[0] = "___9.1.1 Baud Rate__";
	menu_art[1] = "1. 19200bps         ";
	menu_art[2] = "2.  9600bps         ";
	menu_art[3] = "3.  4800bps         ";


	mPrintMenu();

}

cMenu911::~cMenu911() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu911::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();

	return NULL;
}

cMenu * cMenu911::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu911::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu911::Function3()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

