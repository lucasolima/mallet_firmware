/*
 * cMenu.cpp
 *
 *  Created on: May 13, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu.hpp"





cMenu::cMenu(cConfigurations& oConfigurations, cMenu* prnt) : parent(prnt), config(oConfigurations)
{
	// TODO Auto-generated constructor stub
	actual_line = 1;
	init_line = 1;
	line_with_arrow = 1;
	function_is_running_flag = 0;
	function_pointer = NULL;
	function_pointer_counter = 0;
	function_key_pressed = 0;
	function_flag_keep_running = 0;
	function_text = 0;

	IP_Mask = {1,1,1,0,1,1,1,0,1,1,1,0,1,1,1};

	KeyboarMap[0].length = 2;
	KeyboarMap[0].keys = {' ','0',' ',' ',' ',' ',' '};
	KeyboarMap[0].ptr = 0;

	KeyboarMap[1].length = 7;
	KeyboarMap[1].keys = {'.','!','?','@','-','_','1'};
	KeyboarMap[1].ptr = 0;

	KeyboarMap[2].length = 4;
	KeyboarMap[2].keys = {'A','B','C','2',' ',' ',' '};
	KeyboarMap[2].ptr = 0;


	KeyboarMap[3].length = 4;
	KeyboarMap[3].keys = {'D','E','F','3',' ',' ',' '};
	KeyboarMap[3].ptr = 0;


	KeyboarMap[4].length = 4;
	KeyboarMap[4].keys = {'G','H','I','4',' ',' ',' '};
	KeyboarMap[4].ptr = 0;

	KeyboarMap[5].length = 4;
	KeyboarMap[5].keys = {'J','K','L','5',' ',' ',' '};
	KeyboarMap[5].ptr = 0;

	KeyboarMap[6].length = 4;
	KeyboarMap[6].keys = {'M','N','O','6',' ',' ',' '};
	KeyboarMap[6].ptr = 0;

	KeyboarMap[7].length = 5;
	KeyboarMap[7].keys = {'P','Q','R','S','7',' ',' '};
	KeyboarMap[7].ptr = 0;

	KeyboarMap[8].length = 4;
	KeyboarMap[8].keys = {'T','U','V','8',' ',' ',' '};
	KeyboarMap[8].ptr = 0;

	KeyboarMap[9].length = 5;
	KeyboarMap[9].keys = {'W','X','Y','Z','9',' ',' '};
	KeyboarMap[9].ptr = 0;


}

cMenu::~cMenu() {
	// TODO Auto-generated destructor stub
}

int cMenu::mGetActualLine()
{

	return actual_line;
}

int cMenu::mGetInitLine()
{
	return init_line;
}

int cMenu::mGetLength()
{
	return length;
}

cMenu * cMenu::mErase()
{
	return this;
}



int cMenu::mGetLineWithArrow()
{
	return line_with_arrow;
}

void cMenu::mPrintMenu()
{

	char cls = 18;
#ifdef CONSOLE
	std::system("clear");
#else
	cout << (char)20;
	cout << cls;
	cout << flush;
#endif
	cout << menu_art[0];
	if(line_with_arrow == 0) cout << "\b<" ;
	cout << flush;
#ifdef CONSOLE
	cout << endl;
#endif
	if(length >= 1)
	{	cout << menu_art[init_line] ;
		if(line_with_arrow == 1) cout << "\b<" ;
		cout << flush;
	}
#ifdef CONSOLE
	cout << endl;
#endif
	if(length >= 2)
	{
		cout << menu_art[init_line + 1];
		if(line_with_arrow == 2) cout << "\b<" ;
		cout << flush;
	}
#ifdef CONSOLE
	cout << endl;
#endif
	if(length >= 3)
	{
		cout << menu_art[init_line + 2] ;
		if(line_with_arrow == 3) cout << "\b<" ;
		cout << flush;
	}


}

cMenu* temp;

cMenu* cMenu::mGoToParent()
{
	parent->mPrintMenu();
	temp = parent;
	delete this;
	return temp;
}

cMenu* cMenu::mGetParent()
{
	return parent;
}

int cMenu::mScrollDown(int n)
{
	if(n == 0 || (actual_line >= length)) return 0;
	if(line_with_arrow <= 2 && line_with_arrow > 0)
	{
		actual_line ++;
		line_with_arrow ++;
	}
	else if(line_with_arrow == 3)
	{
		init_line ++;
		actual_line++;
	}
	mPrintMenu();
	return 1 + mScrollDown(n - 1);
}

int cMenu::mScrollUp(int n)
{
	if(n == 0 || actual_line <= 1) return 0;
	if(line_with_arrow > 1 && line_with_arrow <= 3)
	{
		actual_line --;
		line_with_arrow --;
	}
	else if(line_with_arrow == 1)
	{
		init_line --;
		actual_line--;
	}
	mPrintMenu();
	return 1 + mScrollUp(n - 1);
}

int cMenu::mJumpToLine(int n)
{
	if(n < 1 || n > length) return -1;
	else if(n <= 3)
	{
		actual_line = n;
		init_line = 1;
		line_with_arrow = n;
	}
	else if(n > 3 && n < length - 1)
	{
		actual_line = n;
		init_line = n;
		line_with_arrow = 1;
	}
	else if(n >= length - 1 && n <= length)
	{
		actual_line = n;
		init_line = length - 2;
		line_with_arrow = n + 3 - length;
	}
	mPrintMenu();
	return 0;

}

cMenu * cMenu::mGoToChild()
{
	return this;
}

void cMenu::mPrintMessage(string s)
{


	char cls = 18;

#ifdef CONSOLE
	std::system("clear");
#else
	cout << (char)19;
	cout << cls;
	cout << flush;
#endif

	cout << endl;
	if(s.length() > 20)
		cout << s.substr(0,20) << endl;
	else
		cout << s << endl;

	usleep(1000000);



}

void cMenu::mPrintMessageNoDelay(string s)
{


	char cls = 18;

#ifdef CONSOLE
	std::system("clear");
#else
	cout << (char)19;
	cout << cls;
	cout << flush;
#endif

	cout << endl;
	if(s.length() > 20)
		cout << s.substr(0,20) << endl;
	else
		cout << s << endl;

}

int cMenu::mGetText(char* buf,int buf_actual_size ,int buf_size, char* function_name)
{
	int num = function_key_pressed - 48;
	int index;

//	if(function_pointer_counter != 0)
//	{
//		fprintf(stderr, "Function pointer 0: %d\nFunction pointer 1: %d\nFunction pointer 2: %d\nFunction pointer 3: %d\n",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]);
//	}


	if(function_pointer_counter == 0)
	{
		function_is_running_flag = 1;
		//function_flag_keep_running = 1;
		function_flag_timeout = 0;
		function_text = 1;
		function_pointer_counter = 1;
		function_pointer = new int[4];
		function_key_pressed = 0;
		function_pointer[0] = 0; //flag de aguardando pressionada de botão
		function_pointer[1] = 0; //ponteiro de posicao na msg
		function_pointer[2] = 0; //ultima tecla apertada
		function_pointer[3] = buf_actual_size; //tamanho da bagaça
		mPrintMessage(function_name);
		cout << (char)19 << (char)18 << (char)252 << flush; //Modo 3 linhas, limpa a tela, cursor pisca
		int i;
		for(i = 0; i < 20; i++) cout << ' ' << flush;
		for(i = 0; i < buf_actual_size; i++)
		{
			cout << buf[i] << flush;

		}
		for(i = 0; i < buf_actual_size; i++)
		{
			cout << '\b' << flush;

		}

	}
	else if(function_key_pressed == 59) // esc
	{
		function_is_running_flag = 0;
		function_pointer_counter = 0;
		function_flag_timeout = 0;
		function_text = 0;
		delete function_pointer;
		function_pointer = NULL;
		cout << (char)253 << flush; //n pisca cursor
		function_text = 0;
		mPrintMenu();
	}
	else if(function_key_pressed == 60 && !function_flag_timeout  ) //erase
	{
		function_pointer[2] = function_key_pressed;
		if(function_pointer[1] > 0)
		{

			if(function_pointer[0] == 1)
			{
				cout << " \b" << flush;
				function_pointer[0] = 0;
			}
			else
			{
				cout << "\b \b" << flush;
				function_pointer[1]--;
				function_pointer[3]--;
			}

			if(function_pointer[3] != function_pointer[1])
			{
				int i;
				for(i = function_pointer[1]; i < function_pointer[3] ; i++ )
				{
					cout << buf[i + 1] << flush;
					buf[i] = buf[i + 1];
				}
				buf[i + 1] = ' ';
				cout << " \b" << flush;
				for(i = function_pointer[1]; i < function_pointer[3] ; i++ ) cout << '\b' << flush;
			}
			else
			{
				if(function_pointer[0] == 1)
				{
					buf[function_pointer[3]] = ' ';
				}
				else
				{
					buf[function_pointer[3] + 1] = ' ';
				}
			}
		}

	}
	else if(function_key_pressed == 58 && !function_flag_timeout  ) //seta direita
	{
		function_pointer[2] = function_key_pressed;
		if(function_pointer[1] < function_pointer[3])
		{
			cout << buf[function_pointer[1]] << flush;
			function_pointer[1]++;
		}
	}
	else if(function_key_pressed == 61 && !function_flag_timeout  ) //seta esquerda
	{
		function_pointer[2] = function_key_pressed;
		if(function_pointer[1] > 0)
		{
			if(function_pointer[0] == 1)
			{
				cout << '\b' << flush;
				function_pointer[1]--;
				function_pointer[0] = 0;
			}
			else
			{
				cout << '\b'  << flush;
				function_pointer[1]--;
			}
		}
	}

	else if(function_key_pressed == 62) //enter
	{
		cout << (char)253 << flush; // n pisca cursor
		function_is_running_flag = 0;
		function_pointer_counter = 0;
		function_flag_timeout = 0;
		function_text = 0;
		int temp1 = function_pointer[3];
		int temp2 = function_pointer[0];
		int temp3 = function_pointer[1];
		delete function_pointer;
		function_pointer = NULL;


		function_text = 0;

		if(buf_size <= temp3) buf[temp1+1] = '\0';
		else if(temp2) buf[temp1+1] = '\0';
		else buf[temp1] = '\0';


		if(strlen(function_name) < 19) cout << (char)18 << function_name << ":\n" << buf << endl;
		else if(strlen(function_name) == 19) cout << (char)18 << function_name << ":" << buf << endl;
		else cout << (char)18 << function_name <<  buf << endl;

		usleep(2000000);

		//fprintf(stderr,"Temp1: %d\nTemp2: %d\nTemp3: %d\n",temp1,temp2,temp3);

		//mPrintMessage("    Valor Salvo    ");


		if(buf_size <= temp3) return temp1 + 1;
		else if(temp2) return temp1 + 1;
		else return temp1;

	}

	else if(function_flag_timeout == 1 && function_pointer[0] == 1 && function_pointer[1] < (buf_size - 1)) //timeout
	{
		cout << buf[function_pointer[1]] << flush;
		if(function_pointer[1] == function_pointer[3])
		{
			function_pointer[1]++;
			function_pointer[3]++;
		}
		else
		{
			function_pointer[1]++;
		}
		function_pointer[0] = 0;
		function_flag_timeout = 0;
		function_pointer[2] = 0;
	}
	else if(function_pointer_counter == 1 && function_flag_timeout == 0 )
	{
		if(function_pointer[1] < 160 && num >= 0 && num <= 9)
		{
			//fprintf(stderr,"%d%d%d%d\n",function_key_pressed != function_pointer[2] , function_pointer[2] < 59 , function_pointer[2] > 0 , function_pointer[1] < (buf_size - 1));
			if(function_key_pressed != function_pointer[2] && function_pointer[2] < 59 && function_pointer[2] > 0 && function_pointer[1] < (buf_size - 1)) //nova tecla apertada
			{
				///fprintf(stderr,"a\n");
				KeyboarMap[num].ptr = 0;
				index = function_pointer[2] - 48;
				cout << KeyboarMap[index].keys[KeyboarMap[index].ptr] << flush;
				if(function_pointer[1] == function_pointer[3])
				{
					function_pointer[1]++;
					function_pointer[3]++;
				}
				else
				{
					function_pointer[1]++;
				}
				buf[function_pointer[1]] = KeyboarMap[num].keys[KeyboarMap[num].ptr];
				cout << KeyboarMap[num].keys[KeyboarMap[num].ptr] << (char)8 << flush;
				function_pointer[2] = (int)function_key_pressed;
				function_pointer[0] = 1;
			}
			else if (function_pointer[0] == 1 && function_key_pressed == function_pointer[2] && function_pointer[2] < 59 && function_pointer[2] > 0) //mesma tecla apertada
			{
				///fprintf(stderr,"b\n");
				KeyboarMap[num].ptr = (KeyboarMap[num].ptr + 1) % KeyboarMap[num].length;
				buf[function_pointer[1]] = KeyboarMap[num].keys[KeyboarMap[num].ptr];
				cout << KeyboarMap[num].keys[KeyboarMap[num].ptr] << '\b' << flush;
				function_pointer[2] = (int)function_key_pressed;
				function_pointer[0] = 1;

			}
			else
			{
				///fprintf(stderr,"c\n");
				KeyboarMap[num].ptr = 0;
				buf[function_pointer[1]] = KeyboarMap[num].keys[KeyboarMap[num].ptr];
				cout << KeyboarMap[num].keys[KeyboarMap[num].ptr] << (char)8 << flush;
				function_pointer[2] = (int)function_key_pressed;
				function_pointer[0] = 1;
			}

		}
	}

	return -1;

}


int cMenu::mGetIP(char* buf, char* function_name)
{
	int num = function_key_pressed - 48;
	int index;

//	if(function_pointer_counter != 0)
//	{
//		fprintf(stderr, "Function pointer 0: %d\nFunction pointer 1: %d\nFunction pointer 2: %d\nFunction pointer 3: %d\n",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]);
//	}


	if(function_pointer_counter == 0)
	{
		function_is_running_flag = 1;
		function_pointer_counter = 1;
		function_pointer = new int[1];
		function_key_pressed = 0;
		function_pointer[0] = 0; //ponteiro de posicao na mensagem
		mPrintMessage(function_name);

		cout << (char)19 << (char)18 << (char)252 << flush; //Modo 3 linhas, limpa a tela, cursor pisca
		int i;
		for(i = 0; i < 20; i++) cout << ' ' << flush;
		for(i = 0; i < 15; i++)
		{
			cout << buf[i] << flush;
		}
		for(i = 0; i < 15; i++)
		{
			cout << '\b' << flush;
		}

	}
	else if(function_key_pressed == 59) // esc
	{
		function_is_running_flag = 0;
		function_pointer_counter = 0;
		function_flag_timeout = 0;
		function_text = 0;
		delete function_pointer;
		function_pointer = NULL;
		cout << (char)253 << flush; //n pisca cursor
		function_text = 0;
		mPrintMenu();
	}
	else if(function_key_pressed == 60 && !function_flag_timeout  ) //erase
	{
		if(function_pointer[0] > 0)
		{
			if(function_pointer[0] == 15)
			{
				cout << "0\b" << flush;
				function_pointer[0]--;
			}
			else
			{
				function_pointer[0]--;
				cout << '\b' << flush;
				if(buf[function_pointer[0]] == '.')
				{
					function_pointer[0]--;
					cout << '\b' << flush;
				}
				cout << "0\b" << flush;
				buf[function_pointer[0]] = '0';
			}
		}

	}
	else if(function_key_pressed == 58) //seta direita
	{

		if(function_pointer[0] < 15)
		{
			if(function_pointer[0] != 14)cout << buf[function_pointer[0]] << flush;
			function_pointer[0]++;
			if(buf[function_pointer[0]] == '.')
			{
				cout << '.' << flush;
				function_pointer[0]++;
			}
		}
	}
	else if(function_key_pressed == 61) //seta esquerda
	{

		if(function_pointer[0] > 0)
		{
			if(function_pointer[0] != 15) cout << '\b'  << flush;
			function_pointer[0]--;
			if(buf[function_pointer[0]] == '.')
			{
				cout << "\b" << flush;
				function_pointer[0]--;
			}
		}

	}

	else if(function_key_pressed == 62) //enter
	{
		cout << (char)253 << flush; // n pisca cursor
		function_is_running_flag = 0;
		function_pointer_counter = 0;
		delete function_pointer;
		function_pointer = NULL;
		buf[15] = '\0';
		int i;
		for(i = 0 ; i < 4; i++)
		{
			char buf_temp[4];
			strncpy(buf_temp,&buf[i*4],3);
			buf_temp[3] = '\0';
			if(atoi(buf_temp) > 255 || atoi(buf_temp) < 0 )
			{
				fprintf(stderr,"%s\n",buf_temp);
				mPrintMessage("    IP Invalido    ");
				return 0;
			}
		}

		if(strlen(function_name) < 19) cout << (char)18 << function_name << ":\n" << buf << endl;
		else if(strlen(function_name) == 19) cout << (char)18 << function_name << ":" << buf << endl;
		else cout << (char)18 << function_name <<  buf << endl;

		usleep(2000000);

		return 15;

	}

	else if(function_pointer_counter == 1)
	{
		if(function_pointer[0] < 15 && num >= 0 && num <= 9)
		{
			//fprintf(stderr,"%d%d%d%d\n",function_key_pressed != function_pointer[2] , function_pointer[2] < 59 , function_pointer[2] > 0 , function_pointer[1] < (buf_size - 1));
			if(function_pointer[0] < 15) //nova tecla apertada
			{
				if(buf[function_pointer[0]] == '.')
				{
					cout << '.' << flush;
					function_pointer[0]++;
				}
				cout << num << flush;
				buf[function_pointer[0]] = function_key_pressed;
				function_pointer[0]++;
				if(buf[function_pointer[0]] == '.')
				{
					cout << '.' << flush;
					function_pointer[0]++;
				}

				if(function_pointer[0] == 15) cout << '\b' << flush;
			}
		}
	}

	return -1;
}


void cMenu::mUpdate()
{
	return;
}

void cMenu::mUpdateTime()
{
	return;
}


