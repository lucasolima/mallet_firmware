/*
 * cMenu95.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu95.hpp"


cMenu95::cMenu95(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 4;

	menu_art = new string[length+1];

	menu_art[0] = "______9.5 GPS_______";
	menu_art[1] = "1. Enviar para      ";
	menu_art[2] = "2. Periodo Tx       ";
	menu_art[3] = "3. Porta UDP 1      ";
	menu_art[4] = "4. Porta UDP 2      ";

	mPrintMenu();

}

cMenu95::~cMenu95() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu95::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();
	if(actual_line == 3) return Function4();



	return NULL;
}

cMenu * cMenu95::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu95::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu95::Function3()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu95::Function4()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
