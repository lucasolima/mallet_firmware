/*
 * cMenu7.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu7.hpp"

cMenu7::cMenu7(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;
	menu_art = new string[length + 1];

	menu_art [0] = "______7. Audio______";
	menu_art [1] = "1. Silenciador      ";
	menu_art [2] = "2. Viva-voz         ";

	mPrintMenu();

}

cMenu7::~cMenu7() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu7::mGoToChild()
{
	if(actual_line == 1) return new cMenu71(config,this);
	if(actual_line == 2) return new cMenu72(config,this);

	return NULL;
}
