/*
 * cMenu98.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu98.hpp"

cMenu98::cMenu98(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "__9.8 Funcionamento_";
	menu_art[1] = "1. Potencia         ";
	menu_art[2] = "2. Emprego          ";

	mPrintMenu();

}

cMenu98::~cMenu98() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu98::mGoToChild()
{
	if(actual_line == 1) return new cMenu981(config,this);
	if(actual_line == 2) return new cMenu982(config,this);
	return NULL;
}

