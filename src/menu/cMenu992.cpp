/*
 * cMenu992.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */



#include "../../inc/menu/cMenu992.hpp"


cMenu992::cMenu992(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "____9.9.2 Senhas____";
	menu_art[1] = "1. Power-on         ";
	menu_art[2] = "2. Configuracao     ";


	mPrintMenu();

}

cMenu992::~cMenu992() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu992::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;

}

cMenu * cMenu992::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu992::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
