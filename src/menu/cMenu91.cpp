/*
 * cMenu91.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu91.hpp"

cMenu91::cMenu91(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 7;

	menu_art = new string[length+1];

	menu_art[0] = "______9.1 Modem_____";
	menu_art[1] = "1. Baud Rate        ";
	menu_art[2] = "2. Polaridade Tx    ";
	menu_art[3] = "3. Polaridade Rx    ";
	menu_art[4] = "4. Blocos de Sinc   ";
	menu_art[5] = "5. Ver Config       ";
	menu_art[6] = "6. Tx Tom Sinc      ";
	menu_art[7] = "7. Tx Teste Link    ";

	mPrintMenu();

}

cMenu91::~cMenu91() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu91::mGoToChild()
{
	if(actual_line == 1) return new cMenu911(config,this);
	if(actual_line == 2) return new cMenu912(config,this);
	if(actual_line == 3) return new cMenu913(config,this);
	if(actual_line == 4) return Function4();
	if(actual_line == 5) return Function5();
	if(actual_line == 6) return Function6();
	if(actual_line == 7) return Function7();


	return NULL;
}

cMenu * cMenu91::Function4()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu91::Function5()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu91::Function6()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu91::Function7()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

