/*
 * cMenu972.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu972.hpp"


cMenu972::cMenu972(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "_____9.7.2 Radio____";
	menu_art[1] = "1. Endereco IP      ";
	menu_art[2] = "2. Msc de Rede      ";


	mPrintMenu();

}

cMenu972::~cMenu972() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu972::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;

}

cMenu * cMenu972::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu972::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
