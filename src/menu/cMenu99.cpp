/*
 * cMenu99.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu99.hpp"


cMenu99::cMenu99(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 8;

	menu_art = new string[length+1];

	menu_art[0] = "____9.9 Diversos____";
	menu_art[1] = "1. Versao           ";
	menu_art[2] = "2. Senhas           ";
	menu_art[3] = "3. Relogio          ";
	menu_art[4] = "4. Hora Remota      ";
	menu_art[5] = "5. Tensao (Volts)   ";
	menu_art[6] = "6. Sinal Recebido   ";
	menu_art[7] = "7. Apaga Memória    ";
	menu_art[8] = "8. Bando de Dados   ";


	mPrintMenu();

}

cMenu99::~cMenu99() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu99::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return new cMenu992(config,this);
	if(actual_line == 3) return Function3();
	if(actual_line == 4) return new cMenu994(config,this);
	if(actual_line == 5) return Function5();
	if(actual_line == 6) return Function6();
	if(actual_line == 7) return Function7();
	if(actual_line == 8) return new cMenu998(config,this);
	return NULL;
}

cMenu * cMenu99::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}


cMenu * cMenu99::Function3()
{
	//implementar aqui
	int num = (int)function_key_pressed - 48;
	char buf[30];
	sprintf(buf," __:__:__ __/__/__ ");

	if(function_pointer == NULL)
	{
		function_pointer = new int[6];
		function_pointer_counter = 1;
		function_is_running_flag = 1;
		mPrintMessage(" Digite Hora e Data ");
		mPrintMessageNoDelay(buf);

	}
	else if(function_key_pressed == 59) //esc
	{
		delete function_pointer;
		function_pointer = NULL;
		function_pointer_counter = 0;
		function_is_running_flag = 0;
		mPrintMenu();
	}
	else if(function_key_pressed == 60) //erase
	{
		if(function_pointer_counter > 1)
		{
			function_pointer_counter --;
			if(function_pointer_counter == 1)
			{
				sprintf(buf," __:__:__ __/__/__ ");
				function_pointer[0] = 0;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 2)
			{
				sprintf(buf," %d_:__:__ __/__/__ ",function_pointer[0]/10);
				function_pointer[0] = (function_pointer[0]/10) * 10;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 3)
			{
				sprintf(buf," %02d:__:__ __/__/__ ",function_pointer[0]);
				function_pointer[1] = 0;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 4)
			{
				sprintf(buf," %02d:%d_:__ __/__/__ ",function_pointer[0],function_pointer[1]/10);
				function_pointer[1] = (function_pointer[1]/10) * 10;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 5)
			{
				sprintf(buf," %02d:%02d:__ __/__/__ ",function_pointer[0],function_pointer[1]);
				function_pointer[2] = 0;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 6)
			{
				sprintf(buf," %02d:%02d:%d_ __/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2]/10);
				function_pointer[2] = (function_pointer[2]/10) * 10;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 7)
			{
				sprintf(buf," %02d:%02d:%02d __/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2]);
				function_pointer[3] = 0;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 8)
			{
				sprintf(buf," %02d:%02d:%02d %d_/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]/10);
				function_pointer[3] = (function_pointer[3]/10) * 10;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 9)
			{
				sprintf(buf," %02d:%02d:%02d %02d/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]);
				function_pointer[4] = 0;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 10)
			{
				sprintf(buf," %02d:%02d:%02d %02d/%d_/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4]/10);
				function_pointer[4] = (function_pointer[4]/10) * 10;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 11)
			{
				sprintf(buf," %02d:%02d:%02d %02d/%02d/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4]);
				function_pointer[5] = 0;
				mPrintMessageNoDelay(buf);
			}
			else if(function_pointer_counter == 12)
			{
				sprintf(buf," %02d:%02d:%02d %02d/%02d/%d_ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4],function_pointer[5]/10);
				function_pointer[5] = (function_pointer[5]/10) * 10;
				mPrintMessageNoDelay(buf);
			}

		}

	}
	else if(function_pointer_counter == 1)
	{
		if(num > 2 || num < 0)
		{
			mPrintMessage("Valor Invalido");
			mPrintMessageNoDelay(buf);
		}
		else
		{

			function_pointer[0] = 10 * num;
			sprintf(buf," %d_:__:__ __/__/__ ",function_pointer[0]/10);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 2;
		}
	}
	else if(function_pointer_counter == 2)
	{
		if((function_pointer[0] + num) > 23)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %d_:__:__ __/__/__ ",function_pointer[0]/10);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[0] = function_pointer[0] + num;
			sprintf(buf," %02d:__:__ __/__/__ ",function_pointer[0]);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 3;
		}

	}
	else if(function_pointer_counter == 3)
	{
		if(num > 5)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:__:__ __/__/__ ",function_pointer[0]);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[1] = 10 * num;
			sprintf(buf," %02d:%d_:__ __/__/__ ",function_pointer[0],function_pointer[1]/10);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 4;
		}
	}
	else if(function_pointer_counter == 4)
	{
		if((function_pointer[1] + num) > 59)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:%d_:__ __/__/__ ",function_pointer[0],function_pointer[1]/10);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[1] = function_pointer[1] + num;
			sprintf(buf," %02d:%02d:__ __/__/__ ",function_pointer[0],function_pointer[1]);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 5;
		}
	}
	else if(function_pointer_counter == 5)
	{
		if(num > 5)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:%02d:__ __/__/__ ",function_pointer[0],function_pointer[1]);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[2] = 10 * num;
			sprintf(buf," %02d:%02d:%d_ __/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2]/10);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 6;
		}
	}
	else if(function_pointer_counter == 6)
	{
		if((function_pointer[2]  + num) > 59)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:%02d:%d_ __/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2]/10);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[2] = function_pointer[2] + num;
			sprintf(buf," %02d:%02d:%02d __/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2]);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 7;
		}
	}
	else if(function_pointer_counter == 7)
	{
		if(num > 3)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:%02d:%02d __/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2]);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[3] = 10 * num;
			sprintf(buf," %02d:%02d:%02d %d_/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]/10);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 8;
		}
	}
	else if(function_pointer_counter == 8)
	{
		if((function_pointer[3] + num) > 31)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:%02d:%02d %d_/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]/10);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[3] = function_pointer[3] + num;
			sprintf(buf," %02d:%02d:%02d %02d/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 9;
		}
	}
	else if(function_pointer_counter == 9)
	{
		if(num > 1)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:%02d:%02d %02d/__/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3]);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[4] = num * 10;
			sprintf(buf," %02d:%02d:%02d %02d/%d_/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4]/10);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 10;
		}

	}
	else if(function_pointer_counter == 10)
	{
		if((function_pointer[4] + num) > 12)
		{
			mPrintMessage("Valor Invalido");
			sprintf(buf," %02d:%02d:%02d %02d/%d_/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4]/10);
			mPrintMessageNoDelay(buf);
		}
		else
		{
			function_pointer[4] = function_pointer[4] + num;
			sprintf(buf," %02d:%02d:%02d %02d/%02d/__ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4]);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 11;
		}
	}
	else if(function_pointer_counter == 11)
	{

		function_pointer[5] = 10 * num;
		sprintf(buf," %02d:%02d:%02d %02d/%02d/%d_ ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4],function_pointer[5]/10);
		mPrintMessageNoDelay(buf);
		function_pointer_counter = 12;

	}
	else if(function_pointer_counter == 12)
	{
		function_pointer[5] = function_pointer[5] + num;
		sprintf(buf," %02d:%02d:%02d %02d/%02d/%02d ",function_pointer[0],function_pointer[1],function_pointer[2],function_pointer[3],function_pointer[4],function_pointer[5]);
		mPrintMessage(buf);
		sprintf(buf,"date -s \"20%02d-%02d-%02d %02d:%02d:%02d\"",function_pointer[5],function_pointer[4],function_pointer[3],function_pointer[0],function_pointer[1],function_pointer[2] );
		system(buf);
		system("hwclock -w");
		delete function_pointer;
		function_pointer = NULL;
		function_pointer_counter = 0;
		function_is_running_flag = 0;
		mPrintMessage("    Valor Salvo     ");
		mPrintMenu();
	}

	return this;
}





cMenu * cMenu99::Function5()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu99::Function6()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu99::Function7()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

