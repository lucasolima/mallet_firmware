/*
 * cMenu0.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu0.hpp"


cMenu0::cMenu0(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt) {
	// TODO Auto-generated constructor stub
	length = 11;
	menu_art = new string[length+1];

	menu_art[0] = "______Menu Raiz_____";
	menu_art[1] = "1. Mensagens        ";
	menu_art[2] = "2. Canais           ";
	menu_art[3] = "3. Chamada Seletiva ";
	menu_art[4] = "4. Salto em Freq    ";
	menu_art[5] = "5. GPS              ";
	menu_art[6] = "6. Luz do Visor     ";
	menu_art[7] = "7. Audio            ";
	menu_art[8] = "8. Agenda           ";
	menu_art[9] = "9. Configuracoes    ";
	menu_art[10]= "10. Bluetooth       ";
	menu_art[11]= "11. Ethernet        ";


	mPrintMenu();

}

cMenu0::~cMenu0() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu0::mGoToChild()
{
	if(actual_line == 1) return new cMenu1(config,this);
	if(actual_line == 2) return new cMenu2(config,this);
	if(actual_line == 3) return Function3();
	if(actual_line == 4) return new cMenu4(config,this);
	if(actual_line == 5) return Function5();
	if(actual_line == 6) return new cMenu6(config,this);
	if(actual_line == 7) return new cMenu7(config,this);
	if(actual_line == 8) return new cMenu8(config,this);
	if(actual_line == 9) return new cMenu9(config,this);
	if(actual_line == 10)return new cMenu10(config,this);
	if(actual_line == 11)return new cMenu11(config,this);

	return NULL;
}

cMenu * cMenu0::Function3()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

#define INDENT_SPACES "  "

cMenu * cMenu0::Function5()
{
	//implementar aqui
	//ifstream InFile("/dev/gps",ios::in);
	int fd = open("/dev/gps",O_RDONLY);
	char* buf; //[32767];
	char* line;
	char* buf_print;
	mPrintMessageNoDelay("        GPS        ");


	buf = new char[32767];
	buf_print = new char[100];
	int size = read(fd,buf,32767);
	int count = 0;
	int i = 0, j = 0,z = 0;
	while(size == 0)
	{
		count++;
		close(fd);
		cout <<buf <<flush;
		fd = open("/dev/gps",O_RDONLY);
		usleep(100000);
		size = read(fd,buf,32767);

		if(count > 20)
		{
			mPrintMessage("Dados indisponiveis");
			buf[z+1] = '\0';
			size = 1;
		}
	}
	//cout << size << endl;
	//cout << buf << endl;
	fprintf(stderr,"%s",buf);


	line = new char[MINMEA_MAX_LENGTH];
	while(buf[z+1] != '\0' )
	{
		//line = new char[MINMEA_MAX_LENGTH];
		for(j = 0 ; j < 80 ; j++)
		{
			//cout << (int)buf[z + j] << endl;
			line[j] = buf[z + j];
			if(buf[z + j] == '\r')
			{
				line[j] = '\n';
				line[j+1] = '\0';
				z = z + j + 2;
				j = 80; //break
			}


		}
		i++;
		switch (minmea_sentence_id(line, false)) {
			case MINMEA_SENTENCE_RMC: {
				struct minmea_sentence_rmc frame;
				if (minmea_parse_rmc(&frame, line)) {
					fprintf(stderr,  INDENT_SPACES "$xxRMC: raw coordinates and speed: (%d/%d,%d/%d) %d/%d\n",
							frame.latitude.value, frame.latitude.scale,
							frame.longitude.value, frame.longitude.scale,
							frame.speed.value, frame.speed.scale);

					fprintf(stderr,  INDENT_SPACES "$xxRMC fixed-point coordinates and speed scaled to three decimal places: (%d,%d) %d\n",
							minmea_rescale(&frame.latitude, 1000),
							minmea_rescale(&frame.longitude, 1000),
							minmea_rescale(&frame.speed, 1000));

					fprintf(stderr,  INDENT_SPACES "$xxRMC floating point degree coordinates and speed: (%f,%f) %f\n",
							minmea_tocoord(&frame.latitude),
							minmea_tocoord(&frame.longitude),
							minmea_tofloat(&frame.speed));

				}
				else {
					fprintf(stderr,  INDENT_SPACES "$xxRMC sentence is not parsed\n");

				}
				cout << (char)20 << (char)18 << "Latitude: " << minmea_tocoord(&frame.latitude) << "\nLongitude: " << minmea_tocoord(&frame.longitude) << "\nSpeed: " << minmea_tofloat(&frame.speed) << endl;

			} break;

			case MINMEA_SENTENCE_GGA: {
				struct minmea_sentence_gga frame;
				if (minmea_parse_gga(&frame, line)) {


					fprintf(stderr,  INDENT_SPACES "$xxGGA: fix quality: %d\n", frame.fix_quality);

				}
				else {
					fprintf(stderr,  INDENT_SPACES "$xxGGA sentence is not parsed\n");

				}
			} break;

			case MINMEA_SENTENCE_GST: {
				struct minmea_sentence_gst frame;
				if (minmea_parse_gst(&frame, line)) {
					fprintf(stderr,  INDENT_SPACES "$xxGST: raw latitude,longitude and altitude error deviation: (%d/%d,%d/%d,%d/%d)\n",
							frame.latitude_error_deviation.value, frame.latitude_error_deviation.scale,
							frame.longitude_error_deviation.value, frame.longitude_error_deviation.scale,
							frame.altitude_error_deviation.value, frame.altitude_error_deviation.scale);

					fprintf(stderr,  INDENT_SPACES "$xxGST fixed point latitude,longitude and altitude error deviation"
						   " scaled to one decimal place: (%d,%d,%d)\n",
							minmea_rescale(&frame.latitude_error_deviation, 10),
							minmea_rescale(&frame.longitude_error_deviation, 10),
							minmea_rescale(&frame.altitude_error_deviation, 10));

					fprintf(stderr,  INDENT_SPACES "$xxGST floating point degree latitude, longitude and altitude error deviation: (%f,%f,%f)",
							minmea_tofloat(&frame.latitude_error_deviation),
							minmea_tofloat(&frame.longitude_error_deviation),
							minmea_tofloat(&frame.altitude_error_deviation));

				}
				else {
					fprintf(stderr,  INDENT_SPACES "$xxGST sentence is not parsed\n");

				}
			} break;

			case MINMEA_SENTENCE_GSV: {
				struct minmea_sentence_gsv frame;
				if (minmea_parse_gsv(&frame, line)) {
					fprintf(stderr,  INDENT_SPACES "$xxGSV: message %d of %d\n", frame.msg_nr, frame.total_msgs);

					fprintf(stderr,  INDENT_SPACES "$xxGSV: sattelites in view: %d\n", frame.total_sats);

					for (int i = 0; i < 4; i++)
					{
						fprintf(stderr,  INDENT_SPACES "$xxGSV: sat nr %d, elevation: %d, azimuth: %d, snr: %d dbm\n",
							frame.sats[i].nr,
							frame.sats[i].elevation,
							frame.sats[i].azimuth,
							frame.sats[i].snr);

					}
				}
				else {
					fprintf(stderr,  INDENT_SPACES "$xxGSV sentence is not parsed\n");

				}
			} break;

			case MINMEA_SENTENCE_VTG: {
			   struct minmea_sentence_vtg frame;
			   if (minmea_parse_vtg(&frame, line)) {
					fprintf(stderr,  INDENT_SPACES "$xxVTG: true track degrees = %f\n",
						   minmea_tofloat(&frame.true_track_degrees));

					fprintf(stderr,  INDENT_SPACES "        magnetic track degrees = %f\n",
						   minmea_tofloat(&frame.magnetic_track_degrees));

					fprintf(stderr,  INDENT_SPACES "        speed knots = %f\n",
							minmea_tofloat(&frame.speed_knots));

					fprintf(stderr,  INDENT_SPACES "        speed kph = %f\n",
							minmea_tofloat(&frame.speed_kph));

			   }
			   else {
					fprintf(stderr,  INDENT_SPACES "$xxVTG sentence is not parsed\n");

			   }
			} break;

			case MINMEA_INVALID: {
				fprintf(stderr,  INDENT_SPACES "$xxxxx sentence is not valid\n");

			} break;

			default: {
				fprintf(stderr,  INDENT_SPACES "$xxxxx sentence is not parsed\n");

			} break;
		}

		//delete line;
	}




	delete buf;
	delete line;
	delete buf_print;

	close(fd);
	usleep(2000000);
	mPrintMenu();
	return this;
}

