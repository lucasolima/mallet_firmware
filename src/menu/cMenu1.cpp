/*
 * cMenu1.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu1.hpp"

#define INBOX_DB  "/flash_partition/msg_inbox_db.json"
#define OUTBOX_DB "/flash_partition/msg_outbox_db.json"

cMenu1::cMenu1(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt) {
	// TODO Auto-generated constructor stub
	length = 5;

	menu_art = new string[length+1];

	menu_art[0] = "____1. Mensagens____";
	menu_art[1] = "1. Enviar Nova      ";
	menu_art[2] = "2. Recebidas        ";
	menu_art[3] = "3. Transmitidas     ";
	menu_art[4] = "4. Caixa de Saida   ";
	menu_art[5] = "5. Pre-progamadas   ";


	ifstream InFile1(INBOX_DB);

	if(!InFile1.good())
	{

		perror("Opening New Inbox Msg Database\n");
		ofstream OutFile1(INBOX_DB);
		OutFile1 << MsgInbox;
		OutFile1.close();

	}
	else
	{
		InFile1 >> MsgInbox;
		InFile1.close();
	}

	ifstream InFile2(OUTBOX_DB);

	if(!InFile2.good())
	{

		perror("Opening New Outbox Msg Database\n");
		ofstream OutFile2(OUTBOX_DB);
		OutFile2 << MsgOutbox;
		OutFile2.close();

	}
	else
	{
		InFile2 >> MsgOutbox;
		InFile2.close();
	}


	mPrintMenu();
}

cMenu1::~cMenu1() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu1::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();
	if(actual_line == 4) return Function4();
	if(actual_line == 5) return Function5();

	return NULL;
}

cMenu * cMenu1::Function1()
{
	int num = function_key_pressed - 48;
	int index;
	//implementar aqui
	if(function_pointer_counter == 0)
	{
		text = new string;
		text->push_back(' ');
		text->push_back(' ');
		function_is_running_flag = 1;
		function_pointer_counter = 1;
		function_pointer = new int[4];
		function_pointer[0] = 0; //flag de aguardando pressionada de botão
		function_pointer[1] = 0; //ponteiro de posicao na msg
		function_pointer[2] = 0; //ultima tecla apertada
		function_pointer[3] = 0; //tamanho da bagaça
		mPrintMessage("   Entre com a Msg  ");
		cout << (char)20 << (char)18 << (char)252 << flush; //Modo 4 linhas, limpa a tela, cursor pisca
		function_text = 1;

	}
	else if(function_key_pressed == 59) //esc
	{
		function_is_running_flag = 0;
		function_pointer_counter = 0;
		function_flag_timeout = 0;
		function_text = 0;
		delete function_pointer;
		delete text;
		function_pointer = NULL;
		cout << (char)253 << flush;
		function_text = 0;
		mPrintMenu();
	}
	else if(function_key_pressed == 60 && !function_flag_timeout  )
	{
		function_pointer[2] = function_key_pressed;
		if(function_pointer[0] == 1)
		{
			cout << ' ' << (char)8 << flush;
			function_pointer[1]--;
			function_pointer[0] = 0;
		}
		else
		{
			cout << (char)8 << ' ' << (char)8 << flush;
			function_pointer[1]--;
		}
	}
	else if(function_key_pressed == 58 && !function_flag_timeout  ) //seta direita
	{
		function_pointer[2] = function_key_pressed;
		if(function_pointer[1] < function_pointer[3])
		{
			//cout << MsgBuffer["Text"].asCString()[function_pointer[1]] << flush;
			cout << text->c_str()[function_pointer[1]] << flush;
			function_pointer[1]++;
		}
	}
	else if(function_key_pressed == 61 && !function_flag_timeout  ) //seta esquerda
	{
		function_pointer[2] = function_key_pressed;
		if(function_pointer[1] > 0)
		{
			if(function_pointer[0] == 1)
			{
				cout << (char)8 << flush;
				function_pointer[1]--;
				function_pointer[0] = 0;
			}
			else
			{
				cout << (char)8 << flush;
				function_pointer[1]--;
			}
		}
	}

	else if(function_key_pressed == 62) //enter
	{
		function_is_running_flag = 0;
		function_pointer_counter = 0;
		function_flag_timeout = 0;
		function_text = 0;
		MsgBuffer["Length"] = function_pointer[3] + 1;
		delete function_pointer;
		MsgBuffer["Text"] = text->c_str();
		delete text;
		function_pointer = NULL;

		cout << (char)253 << flush;
		function_text = 0;

		time_t rawtime;
		struct tm *timeinfo;
		time(&rawtime);
		timeinfo = localtime(&rawtime);
		char buffer[80];

		strftime(buffer,80,"Mensagem enviada as %H:%M:%S - %d-%m-%Y",timeinfo);

		MsgBuffer["Timestamp"] = buffer;
		MsgBuffer["Read"] = 0;
		MsgOutbox.append(MsgBuffer);//config.RadConfig.OutcomeMessageCounter++;
		UpdateOutboxFile();
		//Gerar_Arquivo,  Salvar e enviar =]

		mPrintMessage("  Mensagem Enviada  ");
		mPrintMenu();

	}

	else if(function_flag_timeout == 1 && function_pointer[0] == 1)
	{
		//cout << MsgBuffer["Text"].asCString()[function_pointer[1]] << flush;
		cout << text->c_str()[function_pointer[1]] << flush;
		function_pointer[1]++;
		function_pointer[3]++;
		function_pointer[0] = 0;
		function_flag_timeout = 0;
		function_pointer[2] = 0;
	}
	else if(function_pointer_counter == 1 && function_flag_timeout == 0 )
	{
		if(function_pointer[1] < 160 && num >= 0 && num <= 9)
		{

			if(function_key_pressed != function_pointer[2] && function_pointer[2] < 59 && function_pointer[2] > 0) //nova tecla apertada
			{
				KeyboarMap[num].ptr = 0;
				index = function_pointer[2] - 48;
				cout << KeyboarMap[index].keys[KeyboarMap[index].ptr] << flush;
				function_pointer[1]++;
				function_pointer[3]++;
				//MsgBuffer["Text"][function_pointer[1]] = KeyboarMap[num].keys[KeyboarMap[num].ptr];
				text->push_back(KeyboarMap[num].keys[KeyboarMap[num].ptr]);
				cout << KeyboarMap[num].keys[KeyboarMap[num].ptr] << (char)8 << flush;
				function_pointer[2] = (int)function_key_pressed;
				function_pointer[0] = 1;
			}
			else if (function_pointer[0] == 1 && function_key_pressed == function_pointer[2] && function_pointer[2] < 59 && function_pointer[2] > 0)
			{
				KeyboarMap[num].ptr = (KeyboarMap[num].ptr + 1) % KeyboarMap[num].length;
				//MsgBuffer["Text"][function_pointer[1]] = KeyboarMap[num].keys[KeyboarMap[num].ptr];
				text->at(function_pointer[1]) = KeyboarMap[num].keys[KeyboarMap[num].ptr];
				cout << KeyboarMap[num].keys[KeyboarMap[num].ptr] << (char)8 << flush;
				function_pointer[2] = (int)function_key_pressed;
				function_pointer[0] = 1;

			}
			else
			{
				KeyboarMap[num].ptr = 0;
				text->at(function_pointer[1]) = KeyboarMap[num].keys[KeyboarMap[num].ptr];
				cout << KeyboarMap[num].keys[KeyboarMap[num].ptr] << (char)8 << flush;
				function_pointer[2] = (int)function_key_pressed;
				function_pointer[0] = 1;
			}

		}
	}






	//mPrintMenu();
	return this;
}

cMenu * cMenu1::Function2()
{
	//implementar aqui
	char buf[21];
	string* list_art;
	list_art = new string[MsgInbox.size() + 1];
	int i,j;
	for(i = 0; i < MsgInbox.size(); i++ )
	{
		for(j = 0; j< 18; j++)
		{
			if(j < MsgInbox[i]["Length"].asInt())
			{
				buf[j] = MsgInbox[i]["Text"].asCString()[j];
			}
			else
			{
				buf[j] = ' ';
			}

		}

		buf[18] = (!MsgInbox[i]["Read"].asBool()) * '*' + (MsgInbox[i]["Read"].asBool()) * ' ' ;
		buf[19] = ' ';
		list_art[i+1].assign(buf,20);
	}
	sprintf(buf,"___1.1.2 Recebidas__");
	list_art[0].assign(buf,20);
	return new cMenuText(config, this, list_art, MsgInbox, INBOX_DB);

}

cMenu * cMenu1::Function3()
{
	//implementar aqui
	char buf[21];
	string* list_art;
	list_art = new string[MsgOutbox.size() + 1];
	int i,j;
	for(i = 0; i < MsgOutbox.size(); i++ )
	{
		for(j = 0; j< 18; j++)
		{
			if(j < MsgOutbox[i]["Length"].asInt())
			{
				buf[j] = MsgOutbox[i]["Text"].asCString()[j];
			}
			else
			{
				buf[j] = ' ';
			}

		}

		buf[18] = (!MsgOutbox[i]["Read"].asBool()) * '*' + (MsgOutbox[i]["Read"].asBool()) * ' ' ;
		buf[19] = ' ';
		list_art[i+1].assign(buf,20);
	}
	sprintf(buf,"_1.1.3 Transmitidas_");
	list_art[0].assign(buf,20);
	return new cMenuText(config, this, list_art, MsgOutbox, OUTBOX_DB);

}

cMenu * cMenu1::Function4()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu1::Function5()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

void cMenu1::UpdateOutboxFile()
{
	ofstream OutFile(OUTBOX_DB);
	OutFile << MsgOutbox;
	OutFile.close();
}

void cMenu1::UpdateInboxFile()
{
	ofstream OutFile(INBOX_DB);
	OutFile << MsgInbox;
	OutFile.close();
}

