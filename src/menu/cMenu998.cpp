/*
 * cMenu998.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu998.hpp"


cMenu998::cMenu998(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 4;

	menu_art = new string[length+1];

	menu_art[0] = "9.9.8 Banco de Dados";
	menu_art[1] = "1. Enviar BD        ";
	menu_art[2] = "2. Receber BD       ";
	menu_art[3] = "3. Log Clonagem     ";
	menu_art[4] = "4. Missao           ";

	mPrintMenu();

}

cMenu998::~cMenu998() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu998::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();
	if(actual_line == 4) return Function4();



	return NULL;
}

cMenu * cMenu998::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu998::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu998::Function3()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu998::Function4()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
