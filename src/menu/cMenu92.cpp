/*
 * cMenu922.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu92.hpp"

cMenu92::cMenu92(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "______9.2 Audio_____";
	menu_art[1] = "1. Digital/FM Analog";
	menu_art[2] = "2. Vocoder          ";

	mPrintMenu();

}

cMenu92::~cMenu92() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu92::mGoToChild()
{
	if(actual_line == 1) 	  return new cMenu921(config,this);
	else if(actual_line == 2) return new cMenu922(config,this);
	return NULL;
}

