/*
 * cMenu23.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu23.hpp"

cMenu23::cMenu23(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt) {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "_____2.3 Modos______";
	menu_art[1] = "1. Usuario Unico    ";
	menu_art[2] = "2. TDM              ";

	mPrintMenu();

}

cMenu23::~cMenu23() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu23::mGoToChild()
{
	if(actual_line == 1)
	{
		config.Root["Settings"]["TimeDivisionMultiplex"] = 0;
		config.UpdateFile();
		mPrintMessage("Canal Simples Salvo");
		parent->mPrintMenu();
		return mGoToParent();
	}
	else if(actual_line == 2)
	{
		config.Root["Settings"]["TimeDivisionMultiplex"] = 1;
		config.UpdateFile();
		mPrintMessage("Canal Multiplo Salvo");
		parent->mPrintMenu();
		return mGoToParent();
	}


	return NULL;
}

cMenu * cMenu23::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu23::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}


