/*
 * cMenu8.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu8.hpp"

cMenu8::cMenu8(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 3;
	menu_art = new string[length + 1];

	menu_art[0] = "_____8. Agenda______";
	menu_art[1] = "1. Novo contato     ";
	menu_art[2] = "2. Ver contatos     ";
	menu_art[3] = "3. Meus Dados       ";

	mPrintMenu();
}

cMenu8::~cMenu8() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu8::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();

	return NULL;
}


cMenu * cMenu8::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu8::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu8::Function3()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
