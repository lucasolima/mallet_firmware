/*
 * cMenu982.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu982.hpp"


cMenu982::cMenu982(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "____9.8.2 Emprego___";
	menu_art[1] = "1. Veicular         ";
	menu_art[2] = "2. 50 Ohms          ";

	mPrintMenu();

}

cMenu982::~cMenu982() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu982::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;

}

cMenu * cMenu982::Function1()
{
	//implementar aqui
	config.Root["Settings"]["isVeicular"] = 1;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo    ");
	mPrintMenu();
	return this;
}

cMenu * cMenu982::Function2()
{
	//implementar aqui
	config.Root["Settings"]["isVeicular"] = 0;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo    ");
	mPrintMenu();
	return this;
}
