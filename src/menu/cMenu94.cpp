/*
 * cMenu94.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu94.hpp"

cMenu94::cMenu94(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 3;

	menu_art = new string[length+1];

	menu_art[0] = "_9.4 Acesso ao Meio_";
	menu_art[1] = "1. Persistencia     ";
	menu_art[2] = "2. Retardo Tx       ";
	menu_art[3] = "3. Janela  Tx       ";

	mPrintMenu();

}

cMenu94::~cMenu94() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu94::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();



	return NULL;
}

cMenu * cMenu94::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu94::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu94::Function3()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

