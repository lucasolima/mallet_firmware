/*
 * cMainMenu.cpp
 *
 *  Created on: May 18, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMainMenu.hpp"

cMainMenu::cMainMenu(cConfigurations& oConfigurations, cMenu* prnt)  : cMenu(oConfigurations, prnt) {
	// TODO Auto-generated constructor stub
	length = 3;
	menu_art = new string[3];
	strcpy(TDM_count,"TimeSlot0");
	mUpdate();



}

void cMainMenu::mUpdate()
{
	char buf[20];

	ch = config.Root["ActiveChannel"].asInt();
	cout << (char)253 << flush;
	if(!config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		tx = config.Root["Channels"][config.Root["ActiveChannel"].asUInt()]["FqTx"].asInt() / 1000.0;
		rx = config.Root["Channels"][config.Root["ActiveChannel"].asUInt()]["FqRx"].asInt() / 1000.0;
	}

	else if(config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		tx = config.Root["Channels"][config.Root["Tdm"][TDM_count].asUInt()]["FqTx"].asInt() / 1000.0;
		rx = config.Root["Channels"][config.Root["Tdm"][TDM_count].asUInt()]["FqRx"].asInt() / 1000.0;
	}
	switch(config.Root["Settings"]["PowerAmplifier"].asInt())
	{
		case Low:
			if(config.Root["Settings"]["IsVeicular"].asBool())
				pot = 1;
			else
				pot = 1;
			break;
		case MediumLow:
			if(config.Root["Settings"]["IsVeicular"].asBool())
				pot = 10;
			else
				pot = 2;
			break;
		case MediumHigh:
			if(config.Root["Settings"]["IsVeicular"].asBool())
				pot = 25;
			else
				pot = 5;
			break;
		case High:
			if(config.Root["Settings"]["IsVeicular"].asBool())
				pot = 50;
			else
				pot = 10;
			break;
		default:
			break;

	}

	ptt1 = config.Root["PTT"]["0"].asBool();
	ptt2 = config.Root["PTT"]["1"].asBool();
	battery = 240 + config.Root["Settings"]["BatteryLevel"].asInt();
	rssi = 244 + config.Root["Settings"]["Rssi"].asInt();


	mUpdateTime();
	message = 0; //config.RadConfig.Message; //implementar acesso ao arquivo
	bluetooth = config.Root["Bluetooth"]["StateOnOff"].asBool();//implementar acesso ao arquivo
	if(!config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		sprintf(buf, "%s       %c %c %c %c%c",radio_time,(char)(249 * message + 32 * (!message)),(char)(250 * bluetooth + 32 * (!bluetooth)),(char)battery,(char)rssi,(char)248);
		menu_art[0].assign(&buf[0],20);
		if(ptt1 == 0 && ptt2 == 0) sprintf(&buf[0],"%02d %2.3f %2.3f %2dW",ch,tx,rx,pot);
		else if(ptt1 == 1 || ptt2 == 1) sprintf(&buf[0],"%02d %2.3f  PTT   %2dW",ch,tx,pot);
		menu_art[1].assign(buf,20);
		menu_art[2] = "Ch Tx MHz Rx MHz Pwr";
	}
	else if(config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		sprintf(buf, "%s       %c %c %c %c%c",radio_time,(char)(249 * message + 32 * (!message)),(char)(250 * bluetooth + 32 * (!bluetooth)),(char)battery,(char)rssi,(char)248);
		menu_art[0].assign(&buf[0],20);

		int TS = atoi(&TDM_count[8]);

		if(ptt1 == 0 && ptt2 == 0) sprintf(&buf[0],"T%d %2.3f %2.3f %2dW",TS,tx,rx,pot);
		else if(ptt1 == 1 || ptt2 == 1) sprintf(&buf[0],"T%d %2.3f  PTT  %2dW",TS,tx,pot);
		menu_art[1].assign(buf,20);
		menu_art[2] = "Ts Tx MHz Rx MHz Pwr";
	}
	mPrintMenu();
}

cMainMenu::~cMainMenu() {
	// TODO Auto-generated destructor stub
	delete menu_art;

}

void cMainMenu::mPrintMenu()
{


	char cls = 251;
#ifdef CONSOLE
	std::system("clear");
#else
	cout << (char)19;
	cout << cls;

	//cout << flush;
#endif

	cout << menu_art[0];

#ifdef CONSOLE
	cout << endl;
#endif
	cout << menu_art[1];

#ifdef CONSOLE
	cout << endl;
#endif
	cout << menu_art[2];

	cout << flush;
#ifdef CONSOLE
	cout << endl;
#endif

}

char cMainMenu::mGetBattery()
{
	if(battery > 3 || battery < 0) return 32;
	return 240 + battery;
}

char cMainMenu::mGetRssi()
{
	if(rssi > 3 || rssi < 0) return 32;
	return 244 + rssi;
}

void cMainMenu::mSetBluetooth()
{
	bluetooth = 1;
}

void cMainMenu::mResetBluetooth()
{
	bluetooth = 0;
}

void cMainMenu::mSetMessage()
{
	message = 1;
}

void cMainMenu::mResetMessage()
{
	message = 0;
}

cMenu * cMainMenu::mGoToChild()
{
	return new cMenu0(config,this);
}

int cMainMenu::mScrollDown(int n)
{
	if(!config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		int NrChMax = config.Root["Channels"].size();
		int NrCh = config.Root["ActiveChannel"].asUInt();
		NrCh = (NrCh + NrChMax - 1) % NrChMax;
		config.Root["ActiveChannel"] = NrCh;
		config.UpdateChannel();
	}
	else
	{
		int TS = atoi(&TDM_count[8]);
		TS = (TS + 3) % 4;
		sprintf(TDM_count, "TimeSlot%d",TS);
	}

	config.UpdateFile();
	mUpdate();
	return 0;
}
int cMainMenu::mScrollUp(int n)
{
	if(!config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		int NrChMax = config.Root["Channels"].size();
		int NrCh = config.Root["ActiveChannel"].asUInt();
		NrCh = (NrCh + 1) % NrChMax;
		config.Root["ActiveChannel"] = NrCh;
		config.UpdateChannel();
	}
	else
	{
		int TS = atoi(&TDM_count[8]);
		TS = (TS + 1) % 4;
		sprintf(TDM_count, "TimeSlot%d",TS);
	}
	config.UpdateFile();
	mUpdate();
	return 0;
}

int cMainMenu::mJumpToLine(unsigned int n)
{
	if(!config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		if(n >= config.Root["Channels"].size() )
			mPrintMessage("   Canal Invalido   ");
		else
		{
			config.Root["ActiveChannel"] = n;
			config.UpdateChannel();
			config.UpdateFile();
			mUpdate();
		}
	}

	return 0;
}

void cMainMenu::mUpdateTime()
{
	time_t t = time(0);
	struct tm *tm2 = localtime(&t);
	strftime(radio_time,6,"%H:%M",tm2);
	return;
}


