/*
 * cMenu926.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu926.hpp"


cMenu926::cMenu926(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "_9.2.6 Ctrl de Fluxo";
	menu_art[1] = "1. Ligado           ";
	menu_art[2] = "2. Desligado        ";


	mPrintMenu();

}

cMenu926::~cMenu926() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu926::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();


	return NULL;

}

cMenu * cMenu926::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu926::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}



