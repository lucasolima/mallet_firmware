/*
 * cMenu22.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu22.hpp"

cMenu22::cMenu22(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 4;

	menu_art = new string[length+1];

	menu_art[0] = "_2.2 PreProgramados_";
	menu_art[1] = "1. Editar           ";
	menu_art[2] = "2. Selecionar       ";
	menu_art[3] = "3. Adicionar        ";
	menu_art[4] = "4. Remover          ";

	mPrintMenu();

}

cMenu22::~cMenu22() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu22::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return new cMenu222(config,this);
	if(actual_line == 3) return Function3();
	if(actual_line == 4) return Function4();


	return NULL;
}

cMenu * cMenu22::Function1()
{
	//implementar aqui
	//implementar aqui
		int num = (int)function_key_pressed - 48;
		char buf[21];
		fprintf(stderr, "Key = %d", num);


		if(function_pointer == NULL && function_pointer_counter == 0)
		{
			function_pointer = new int[4];
			function_pointer[0] = 0;
			function_pointer[1] = 0;
			function_pointer[2] = 0;
			function_pointer[3] = 0;
			sprintf(buf,"  CH: __ Rx/Tx: Rx  ");
			function_pointer_counter = 1;
			function_is_running_flag = 1;
			function_key_pressed = 0;
			mPrintMessage(" Selecione Canal ");
			mPrintMessageNoDelay(buf);

		}
		else if(function_key_pressed == 59) //esc
		{
			delete function_pointer;
			function_pointer = NULL;
			function_pointer_counter = 0;
			function_is_running_flag = 0;

			mPrintMenu();
		}
		else if(function_key_pressed == 60) //erase
		{
			if(function_pointer_counter > 1)
			{
				function_pointer_counter --;
				if(function_pointer_counter == 1)
				{
					function_pointer[0] = 0;
					sprintf(buf,"  CH: __ Rx/Tx: Rx  ");
					mPrintMessageNoDelay(buf);


				}
				else if(function_pointer_counter == 2)
				{
					function_pointer[0] = (function_pointer[0]/10) * 10;
					sprintf(buf,"  CH: %d_ Rx/Tx: Rx  ",function_pointer[0]/10);
					mPrintMessageNoDelay(buf);

				}
				else if(function_pointer_counter == 4)
				{
					function_pointer[2] = 0;
					if(function_pointer[1] == 0) sprintf(buf," CH %02d __.___ MHz Rx ",function_pointer[0]);
					else if(function_pointer[1] == 1) sprintf(buf," CH %02d __.___ MHz Tx ",function_pointer[0]);
					mPrintMessageNoDelay(buf);

				}
				else if(function_pointer_counter == 5)
				{
					function_pointer[2] = (function_pointer[2]/10)*10;
					if(function_pointer[1] == 0) sprintf(buf," CH %02d %d_.___ MHz Rx ",function_pointer[0],function_pointer[2]/10);
					else if(function_pointer[1] == 1) sprintf(buf," CH %02d %d_.___ MHz Tx ",function_pointer[0],function_pointer[2]/10);
					mPrintMessageNoDelay(buf);

				}
				else if(function_pointer_counter == 6)
				{
					function_pointer[3] = 0;
					if(function_pointer[1] == 0) sprintf(buf," CH %02d %02d.___ MHz Rx ",function_pointer[0],function_pointer[2]);
					else if(function_pointer[1] == 1) sprintf(buf," CH %02d %02d.___ MHz Tx ",function_pointer[0],function_pointer[2]);
					mPrintMessageNoDelay(buf);

				}
				else if(function_pointer_counter == 7)
				{
					function_pointer[3] = (function_pointer[3]/100) * 100;
					if(function_pointer[1] == 0) sprintf(buf," CH %02d %02d.%d__ MHz Rx ",function_pointer[0],function_pointer[2],function_pointer[3]/100);
					else if(function_pointer[1] == 1) sprintf(buf," CH %02d %02d.%d__ MHz Tx ",function_pointer[0],function_pointer[2],function_pointer[3]/100);
					mPrintMessageNoDelay(buf);

				}
				else if(function_pointer_counter == 8)
				{
					function_pointer[3] = (function_pointer[3]/10) * 10;
					if(function_pointer[1] == 0) sprintf(buf," CH %02d %02d.%02d_ MHz Rx ",function_pointer[0],function_pointer[2],function_pointer[3]/10);
					else if(function_pointer[1] == 1) sprintf(buf," CH %02d %02d.%02d_ MHz Tx ",function_pointer[0],function_pointer[2],function_pointer[3]/10);
					mPrintMessageNoDelay(buf);

				}


			}
			return this;

		}
		else if(function_pointer_counter == 1 && num < 10)
		{
			if(num > config.Root["Channels"].size()/10)
			{
				mPrintMessage("Valor Invalido");
				mPrintMessageNoDelay(buf);
			}
			else
			{

				function_pointer[0] = num * 10;
				sprintf(buf,"  CH: %d_ Rx/Tx: Rx  ",function_pointer[0]/10);
				mPrintMessageNoDelay(buf);
				function_pointer_counter = 2;
			}
		}
		else if(function_pointer_counter == 2 && num < 10)
		{
			if((function_pointer[0] + num) > config.Root["Channels"].size())
			{
				mPrintMessage("Valor Invalido");
				sprintf(buf,"  CH: %d_ Rx/Tx: Rx  ",function_pointer[0]/10);
				mPrintMessageNoDelay(buf);
			}
			else
			{
				function_pointer[0] += num;
				sprintf(buf,"  CH: %02d Rx/Tx: Rx  ",function_pointer[0]);
				mPrintMessageNoDelay(buf);
				function_pointer_counter = 3;
			}

		}
		else if(function_pointer_counter == 3 )
		{
			if(function_key_pressed != 58 && function_key_pressed != 61 && function_key_pressed != 62)
			{
				//mPrintMessageNoDelay(buf);
			}
			else if(function_key_pressed == 58 || function_key_pressed == 61)
			{
				function_pointer[1] = (function_pointer[1] + 1) % 2;
				if(function_pointer[1] == 1)
				{
					sprintf(buf,"  CH: %02d Rx/Tx: Tx  ",function_pointer[0]);
					mPrintMessageNoDelay(buf);
				}
				else if(function_pointer[1] == 0)
				{
					sprintf(buf,"  CH: %02d Rx/Tx: Rx  ",function_pointer[0]);
					mPrintMessageNoDelay(buf);
				}
			}
			else if(function_key_pressed == 62)
			{
				if(function_pointer[1] == 0) sprintf(buf," CH %02d __.___ MHz Rx ",function_pointer[0]);
				else if(function_pointer[1] == 1) sprintf(buf," CH %02d __.___ MHz Tx ",function_pointer[0]);
				mPrintMessageNoDelay(buf);
				function_pointer_counter = 4;
			}
		}
		else if(function_pointer_counter == 4 && num < 10)
		{

			function_pointer[2] += num * 10;
			if(function_pointer[1] == 0) sprintf(buf," CH %02d %d_.___ MHz Rx ",function_pointer[0],function_pointer[2]/10);
			else if(function_pointer[1] == 1) sprintf(buf," CH %02d %d_.___ MHz Tx ",function_pointer[0],function_pointer[2]/10);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 5;

		}
		else if(function_pointer_counter == 5 && num < 10)
		{

			function_pointer[2] += num;
			if(function_pointer[1] == 0) sprintf(buf," CH %02d %02d.___ MHz Rx ",function_pointer[0],function_pointer[2]);
			else if(function_pointer[1] == 1) sprintf(buf," CH %02d %02d.___ MHz Tx ",function_pointer[0],function_pointer[2]);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 6;
		}
		else if(function_pointer_counter == 6 && num < 10)
		{
			function_pointer[3] = 100 * num;
			if(function_pointer[1] == 0) sprintf(buf," CH %02d %02d.%d__ MHz Rx ",function_pointer[0],function_pointer[2],function_pointer[3]/100);
			else if(function_pointer[1] == 1) sprintf(buf," CH %02d %02d.%d__ MHz Tx ",function_pointer[0],function_pointer[2],function_pointer[3]/100);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 7;

		}
		else if(function_pointer_counter == 7 && num < 10)
		{
			function_pointer[3] += 10 * num;
			if(function_pointer[1] == 0) sprintf(buf," CH %02d %02d.%02d_ MHz Rx ",function_pointer[0],function_pointer[2],function_pointer[3]/10);
			else if(function_pointer[1] == 1) sprintf(buf," CH %02d %02d.%02d_ MHz Tx ",function_pointer[0],function_pointer[2],function_pointer[3]/10);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 8;
		}
		else if(function_pointer_counter == 8 && num < 10)
		{
			function_pointer[3] += num;
			if(function_pointer[1] == 0) sprintf(buf," CH %02d %02d.%03d MHz Rx ",function_pointer[0],function_pointer[2],function_pointer[3]);
			else if(function_pointer[1] == 1) sprintf(buf," CH %02d %02d.%03d MHz Tx ",function_pointer[0],function_pointer[2],function_pointer[3]);
			mPrintMessageNoDelay(buf);

			function_pointer[2] = function_pointer[2] * 1000 + function_pointer[3];
			if((function_pointer[2] % 25) != 0 || function_pointer[2] > 88000  || function_pointer[2] < 30000)
			{
				mPrintMessage("   Valor Invalido   ");
				function_pointer[2] = 0;
				function_pointer[3] = 0;
				function_pointer_counter = 4;
				if(function_pointer[1] == 0) sprintf(buf," CH %02d __.___ MHz Rx ",function_pointer[0]);
				else if(function_pointer[1] == 1) sprintf(buf," CH %02d __.___ MHz Tx ",function_pointer[0]);
				mPrintMessageNoDelay(buf);
			}
			else
			{
				if(function_pointer[1] == 0) config.Root["Channels"][function_pointer[0]]["FqRx"] = function_pointer[2];
				else if(function_pointer[1] == 1) config.Root["Channels"][function_pointer[0]]["FqTx"] = function_pointer[2];
				if(config.Root["Channels"][function_pointer[0]]["FqTx"].isNull())
				{
					char buf[25];
					sprintf(buf, "Rede%d",function_pointer[0]+1);
					config.Root["Channels"][function_pointer[0]]["Name"]	   	 = buf;

					config.Root["Channels"][function_pointer[0]]["Vocoder"]      = TWELP;
					config.Root["Channels"][function_pointer[0]]["Modulation"]	 = FSK4;
					config.Root["Channels"][function_pointer[0]]["AudioType"]	 = "Digital";

					sprintf(buf, "192.168.%d.1",function_pointer[0]+1);
					config.Root["Channels"][function_pointer[0]]["Ip"]	 		 = buf;

					config.Root["Channels"][function_pointer[0]]["Netmask"]	 	 = "255.255.255.0";
					config.Root["Channels"][function_pointer[0]]["Crypto"]	     = None;
					config.Root["Channels"][function_pointer[0]]["Key"]		 	 = 0;
				}
				config.UpdateFile();

				delete function_pointer;
				function_pointer = NULL;
				function_pointer_counter = 0;
				function_is_running_flag = 0;
				mPrintMessage("    Valor Salvo     ");
				if(!function_flag_keep_running) mPrintMenu();

			}


		}

	return this;
}

//cMenu * cMenu22::Function2()
//{
//	//implementar aqui
//	mPrintMessage("To Be Implemented...");
//	mPrintMenu();
//	return this;
//}

cMenu * cMenu22::Function3()
{
	//implementar aqui

	char buf[20];
	if(config.Root["Channels"].size() >= 100 &&  function_pointer == NULL)
	{
		mPrintMessage("   Adicionar Canal  ");
		mPrintMessage("Nro de canais > 100");
		return this;
	}
	else if(function_pointer == NULL && function_pointer_counter == 0)
	{
		mPrintMessage("   Adicionar Canal  ");
		function_pointer_counter = 1;
		function_is_running_flag = 1;
		function_pointer= new int[4];
		function_pointer_counter = 4;
		function_count = 0;
		//config.RadConfig.ChannelsCounter++;
		config.UpdateFile();
		function_pointer[0] = config.Root["Channels"].size();
		function_pointer[1] = 0;
		function_pointer[2] = 0;
		function_pointer[3] = 0;
		function_flag_keep_running = 1;

		sprintf(buf," CH %02d __.___ MHz Rx ",function_pointer[0]);

		mPrintMessageNoDelay(buf);
		//Function1();

	}
	else if(function_key_pressed == 59)
	{
		if(function_pointer != NULL)
		{
			delete function_pointer;
			function_pointer = NULL;
		}
		//config.RadConfig.ChannelsCounter--;
		function_pointer_counter = 0;
		function_is_running_flag = 0;
		function_count = 0;
		function_flag_keep_running = 0;
	}
	else if(function_is_running_flag == 1)
	{
		Function1();
		if(function_is_running_flag == 0 && function_count == 0)
		{
			function_count++;
			function_is_running_flag = 1;
			function_pointer= new int[4];
			function_pointer_counter = 4;
			function_pointer[0] = config.Root["Channels"].size() - 1;
			function_pointer[1] = 1;
			function_pointer[2] = 0;
			function_pointer[3] = 0;
			function_flag_keep_running = 1;

			sprintf(buf," CH %02d __.___ MHz Tx ",function_pointer[0]);
			mPrintMessageNoDelay(buf);
		}
		else if(function_is_running_flag == 0)
		{
			function_count = 0;
			function_flag_keep_running = 0;
			mPrintMenu();

		}

	}

	return this;
}

cMenu * cMenu22::Function4()
{
	char buf[20];
	int num = function_key_pressed - 48;
	//implementar aqui
	if(function_pointer_counter == 0 && function_pointer == NULL)
	{
		function_pointer = new int[2];
		function_is_running_flag = 1;
		function_pointer_counter = 1;
		mPrintMessage("   Deletar Canal   ");
		sprintf(buf,"     Canal: __     ");
		mPrintMessageNoDelay(buf);
	}
	else if(function_key_pressed == 59)
	{
		delete function_pointer;
		function_pointer = NULL;
		function_is_running_flag = 0;
		function_pointer_counter = 0;

	}
	else if(function_key_pressed == 60)
	{
		if(function_pointer_counter > 1)
		{
			function_pointer_counter--;
			if(function_pointer_counter == 1)
			{
				function_pointer[0] = 0;
				sprintf(buf,"     Canal: __     ");
				mPrintMessageNoDelay(buf);
			}
		}
	}
	else if(function_pointer_counter == 1 && function_key_pressed > 47 && function_key_pressed < 58)
	{
		function_pointer[0] = num;
		sprintf(buf,"     Canal: %d_     ", function_pointer[0]);
		mPrintMessageNoDelay(buf);
		function_pointer_counter = 2;

	}
	else if(function_pointer_counter == 2 && function_key_pressed > 47 && function_key_pressed < 58)
	{
		function_pointer[1] = num;
		sprintf(buf,"     Canal: %d%d     ", function_pointer[0],function_pointer[1]);
		mPrintMessageNoDelay(buf);
		if((function_pointer[0] * 10 + function_pointer[1]) > config.Root["Channels"].size())
		{
			mPrintMessage("   Canal Invalido   ");
			function_pointer[0] = 0;
			function_pointer[1] = 0;
			sprintf(buf,"     Canal: __     ");
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 1;

		}
		else
		{
			sprintf(buf,"Confirma Canal %d%d?",function_pointer[0],function_pointer[1]);
			mPrintMessageNoDelay(buf);
			function_pointer_counter = 3;
		}


	}

	else if(function_pointer_counter == 3)
	{
		if(function_key_pressed == 62)
		{
			int ch = function_pointer[0] * 10 + function_pointer[1];
			int i;
			Json::Value temp;
			for(i = 0; i < config.Root["Channels"].size() - 1; i++ )
			{
				if(i < ch)
					temp["Channels"].append(config.Root["Channels"][i]);
				else if(i >= ch)
					temp["Channels"].append(config.Root["Channels"][i + 1]);
			}
			//config.RadConfig.ChannelsCounter -- ;
			config.Root.removeMember("Channels");
			for(i = 0; i < temp["Channels"].size(); i++ )
			{

				if(strncmp(temp["Channels"][i]["Name"].asCString(),"Rede",4) == 0 )
				{
					char buftemp[7];
					sprintf(buftemp, "Rede%d", i);
					temp["Channels"][i]["Name"] = buftemp;
				}
				if(strncmp(temp["Channels"][i]["Ip"].asCString(),"192.168.",8) == 0 )
				{
					char buftemp[20];
					sprintf(buftemp, "192.168.%d.1", i + 1);
					temp["Channels"][i]["Ip"] = buftemp;
				}
				config.Root["Channels"].append(temp["Channels"][i]);

			}
			if(config.Root["ActiveChannel"].asInt() > ch)
			{
				config.Root["ActiveChannel"] = config.Root["ActiveChannel"].asInt() - 1;
			}
			if(config.Root["Tdm"]["TimeSlot0"].asInt() > ch)
			{
				config.Root["Tdm"]["TimeSlot0"] = config.Root["Tdm"]["TimeSlot0"].asInt() - 1;
			}
			if(config.Root["Tdm"]["TimeSlot1"].asInt() > ch)
			{
				config.Root["Tdm"]["TimeSlot1"] = config.Root["Tdm"]["TimeSlot1"].asInt() - 1;
			}
			if(config.Root["Tdm"]["TimeSlot2"].asInt() >  ch)
			{
				config.Root["Tdm"]["TimeSlot2"] = config.Root["Tdm"]["TimeSlot2"].asInt() - 1;
			}
			if(config.Root["Tdm"]["TimeSlot3"].asInt() >  ch)
			{
				config.Root["Tdm"]["TimeSlot3"] = config.Root["Tdm"]["TimeSlot3"].asInt() - 1;
			}
			if(config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
			{
				config.UpdateTdmChannel();
			}
			else
			{
				config.UpdateChannel();
			}
			config.UpdateFile();
			mPrintMessage("   Canal Apagado    ");
		}
		else
		{
			mPrintMessage(" Operacao Cancelada ");
		}
		function_pointer_counter = 0;
		delete function_pointer;
		function_pointer = NULL;
		function_is_running_flag = 0;
		function_pointer_counter = 0;
		mPrintMenu();

	}

	return this;
}

