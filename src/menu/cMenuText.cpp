/*
 * cMenuText.cpp
 *
 *  Created on: May 25, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenuText.hpp"

cMenuText::cMenuText(cConfigurations& oConfigurations, cMenu* prnt, string* list_art, Json::Value& BoxMsg, const char* path) : cMenu(oConfigurations, prnt) , MsgBuf(BoxMsg)  {
	// TODO Auto-generated constructor stub
	length = BoxMsg.size();
	menu_art = list_art;
	//MsgBuf = BoxMsg;
	Path = path;
	mPrintMenu();
}

cMenuText::~cMenuText() {
	// TODO Auto-generated destructor stub

}


cMenu * cMenuText::mGoToChild()
{
	int i,x = actual_line - 1;
	MsgBuf[x]["Read"] = 1;
	menu_art[actual_line][18] = ' ';

	cout << (char)18 << flush;
	for(i = 0; i < MsgBuf[x]["Length"].asUInt() ; i++ )
	{
		cout << MsgBuf[x]["Text"].asCString()[i] << flush;
	}

	ofstream OutFile(Path);
	OutFile << MsgBuf;
	OutFile.close();

	usleep(2000000);
	mGetParent()->mPrintMenu();
	return mGoToParent();

}

// /flash_partition/

cMenu * cMenuText::mErase()
{
	unsigned int i,num_pos,x = actual_line - 1;

	Json::Value *temp;

	MsgBuf.removeIndex(x,temp);

	ofstream OutFile(Path);
	OutFile << MsgBuf;
	OutFile.close();


	mPrintMessage("Mensagem Apagada");
	mGetParent()->mPrintMenu();
	return mGoToParent();
}
