/*
 * cMenu93.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu93.hpp"

cMenu93::cMenu93(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "__9.3 Salto em Freq_";
	menu_art[1] = "1. Saltos / Seg     ";
	menu_art[2] = "2. Prdo Varredura   ";
	menu_art[3] = "3. Sicronizacao     ";
	menu_art[4] = "4. Chave            ";
	menu_art[5] = "5. Tabela Salto     ";


	mPrintMenu();

}

cMenu93::~cMenu93() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu93::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return new cMenu933(config,this);
	if(actual_line == 4) return Function4();
	if(actual_line == 5) return Function5();


	return NULL;
}

cMenu * cMenu93::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu93::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu93::Function4()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu93::Function5()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
