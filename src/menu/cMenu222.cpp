/*
 * cMenu222.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */


#include "../../inc/menu/cMenu222.hpp"


cMenu222::cMenu222(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 5;

	menu_art = new string[length+1];

	menu_art[0] = "__2.2.2 Selecionar__";
	menu_art[1] = "1. Usuario Simples  ";
	menu_art[2] = "2. Usuario 1 (TDM)  ";
	menu_art[3] = "3. Usuario 2 (TDM)  ";
	menu_art[4] = "4. Usuario 3 (TDM)  ";
	menu_art[5] = "5. Usuario 4 (TDM)  ";

	mPrintMenu();

}

cMenu222::~cMenu222() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu222::mGoToChild()
{
	char buf[21];
	string* list_art;
	list_art = new string[config.Root["Channels"].size() + 1];
	int i;
	float tx,rx;
	cMenu *ret_ptr;
	////ret = new int;

	for(i = 0; i < config.Root["Channels"].size(); i++ )
	{

		tx = config.Root["Channels"][i]["FqTx"].asUInt() / 1000.0;
		rx = config.Root["Channels"][i]["FqRx"].asUInt() / 1000.0;
		sprintf(buf,"CH %2d %2.3f/%2.3f ",i,tx,rx);
		list_art[i+1].assign(buf,20);
	}
	if(actual_line == 1)
	{
		list_art[0] = "Ch    Tx MHz/Rx MHz ";
		ret_ptr = new cMenuList(config, this, list_art, config.Root["Channels"].size(), config.Root["ActiveChannel"]);
		return ret_ptr;

	}
	if(actual_line == 2)
	{
		list_art[0] = "Usr1  Tx MHz/Rx MHz ";
		ret_ptr = new cMenuList(config, this, list_art, config.Root["Channels"].size(), config.Root["Tdm"]["TimeSlot0"]);
		return ret_ptr;

	}
	if(actual_line == 3)
	{
		list_art[0] = "Usr2  Tx MHz/Rx MHz ";
		ret_ptr =  new cMenuList(config,this,list_art,config.Root["Channels"].size(), config.Root["Tdm"]["TimeSlot1"]);
		return ret_ptr;
	}
	if(actual_line == 4)
	{
		list_art[0] = "Usr3  Tx MHz/Rx MHz ";
		ret_ptr =  new cMenuList(config,this,list_art,config.Root["Channels"].size(),config.Root["Tdm"]["TimeSlot2"]);
		return ret_ptr;
	}
	if(actual_line == 5)
	{
		list_art[0] = "Usr4  Tx MHz/Rx MHz ";
		ret_ptr = new cMenuList(config,this,list_art,config.Root["Channels"].size(),config.Root["Tdm"]["TimeSlot3"]);
		return ret_ptr;
	}


	return NULL;

}


