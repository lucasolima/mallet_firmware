/*
 * cMenu913.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu913.hpp"


cMenu913::cMenu913(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "_9.1.3 Polaridade Rx";
	menu_art[1] = "1. Normal           ";
	menu_art[2] = "2. Invertida        ";

	mPrintMenu();

}

cMenu913::~cMenu913() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu913::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;

}

cMenu * cMenu913::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu913::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

