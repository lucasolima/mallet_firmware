/*
 * cMenuList.cpp
 *
 *  Created on: May 23, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenuList.hpp"

cMenuList::cMenuList(cConfigurations& oConfigurations, cMenu* prnt, string* list_art, int list_length, Json::Value& parameter) : cMenu(oConfigurations, prnt) , parameter_to_be_changed(parameter){
	// TODO Auto-generated constructor stub

	length = list_length;
	menu_art = list_art;
	parameter_to_be_changed = parameter;
	mPrintMenu();
}

cMenuList::~cMenuList() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenuList::mGoToChild()
{
	parameter_to_be_changed = actual_line - 1;
	if(config.Root["Settings"]["TimeDivisionMultiplex"].asBool())
	{
		config.UpdateTdmChannel();
	}
	else
	{
		config.UpdateChannel();
	}
	config.UpdateFile();
	mPrintMessage("   Valor Salvo   ");
	mGetParent()->mPrintMenu();
	return mGoToParent();

}
