/*
 * cMenu10.cpp
 *
 *  Created on: May 31, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu10.hpp"

cMenu10::cMenu10(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 6;

	menu_art = new string[length+1];

	menu_art[0] = "____10. Bluetooth___";
	if(config.Root["Bluetooth"]["StateOnOff"].asBool()) menu_art[1] = "1. Desligar         ";
	else if(!config.Root["Bluetooth"]["StateOnOff"].asBool()) menu_art[1] = "1. Ligar            ";
	menu_art[2] = "2. Renomear         ";
	menu_art[3] = "3. Editar Pin       ";
	menu_art[4] = "4. Busca/Conectar   ";
	menu_art[5] = "5. IP Gateway BT    ";
	menu_art[6] = "6. Netmask BT       ";


	mPrintMenu();

}

cMenu10::~cMenu10() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu10::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();
	if(actual_line == 4) return new cMenu104(config,this);
	if(actual_line == 5) return Function5();
	if(actual_line == 6) return Function6();


	return NULL;
}


cMenu * cMenu10::Function1()
{
	//implementar aqui
	if(config.Root["Bluetooth"]["StateOnOff"].asBool())
	{
		config.Root["Bluetooth"]["StateOnOff"] = 0;
		config.UpdateFile();
		config.Blue.mTurnOff();
		mPrintMessage(" Bluetooth Desligado ");
	}
	else if(!config.Root["Bluetooth"]["StateOnOff"].asBool())
	{
		config.Root["Bluetooth"]["StateOnOff"] = 1;
		config.UpdateFile();
		config.Blue.mTurnOn();
		mPrintMessage("  Bluetooth Ligado  ");
	}
	cMenu *temp = parent;
	free(this);
	return temp->mGoToChild();
}

cMenu * cMenu10::Function2()
{
	if(function_is_running_flag == 0) strcpy(buff,config.Root["Bluetooth"]["Name"].asCString());
	int i ;
	int ret = mGetText(buff,strlen(buff),20,"Nome Rede Bluetooth");
	if(ret< 0) return this;
	else
	{
		buff[ret] = '\0';
		config.Root["Bluetooth"]["Name"] = buff;
		config.Blue.mChangeName(config.Root["Bluetooth"]["Name"].asCString());
		config.UpdateFile();
		fprintf(stderr,"Nova rede Bluetooth %s\n",config.Root["Bluetooth"]["Name"].asCString());

		mPrintMenu();
		return this;
	}
	mPrintMenu();
	return this;
}

cMenu * cMenu10::Function3()
{
	if(function_is_running_flag == 0) strcpy(buff,config.Root["Bluetooth"]["Pin"].asCString());
	int i ;
	int ret = mGetText(buff,strlen(buff),6,"Pin Rede Bluetooth");
	if(ret< 0) return this;
	else
	{
		buff[ret] = '\0';
		config.Root["Bluetooth"]["Pin"] = buff;
		config.Blue.mChangePin(config.Root["Bluetooth"]["Pin"].asCString());
		config.UpdateFile();
		fprintf(stderr,"Novo Pin Bluetooth %s\n",config.Root["Bluetooth"]["Pin"].asCString());

		mPrintMenu();
		return this;
	}
	mPrintMenu();
	return this;
}



cMenu * cMenu10::Function5()
{
	if(function_is_running_flag == 0) strcpy(buf_ip,config.Root["Bluetooth"]["Ip"].asCString());
	int i ;
	int ret = mGetIP(buf_ip,"Gateway Bluetooth");
	if(ret< 0) return this;
	else
	{
		if(ret == 0)
		{

			mPrintMenu();
			return this;
		}
		strncpy(buf_ip,buf_ip,ret);
		config.Root["Bluetooth"]["Ip"] = buf_ip;
		config.UpdateFile();
		fprintf(stderr,"Novo IP Bluetooth %s\n",config.Root["Bluetooth"]["Ip"].asCString()

		);
		char buf_temp[100];
		strcpy(buf_temp, "ifconfig pan0 ");
		strcat(buf_temp, buf_ip);
		strcat(buf_temp, " > /var/log/dump");
		fprintf(stderr, "%s\n",buf_temp);
		system(buf_temp);

	}
	mPrintMenu();
	return this;
}


cMenu * cMenu10::Function6()
{
	if(function_is_running_flag == 0) strcpy(buf_ip,config.Root["Bluetooth"]["Netmask"].asCString());
	int i ;
	int ret = mGetIP(buf_ip,"Netmask Bluetooth");
	if(ret< 0) return this;
	else
	{
		if(ret == 0)
		{

			mPrintMenu();
			return this;
		}
		strncpy(buf_ip,buf_ip,ret);
		config.Root["Bluetooth"]["Netmask"] = buf_ip;
		config.UpdateFile();
		fprintf(stderr,"Nova Netmask Bluetooth %s\n",config.Root["Bluetooth"]["Netmask"].asCString());
		char buf_temp[100];
		strcpy(buf_temp, "ifconfig pan0 netmask ");
		strcat(buf_temp, buf_ip);
		strcat(buf_temp, " > /var/log/dump");
		fprintf(stderr, "%s\n",buf_temp);
		system(buf_temp);

	}
	mPrintMenu();
	return this;
}



