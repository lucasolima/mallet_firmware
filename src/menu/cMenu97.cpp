/*
 * cMenu97.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu97.hpp"

cMenu97::cMenu97(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 3;

	menu_art = new string[length+1];

	menu_art[0] = "______9.7 Rede______";
	menu_art[1] = "1. Ethernet         ";
	menu_art[2] = "2. Radio            ";
	menu_art[3] = "3. Roteamento       ";

	mPrintMenu();

}

cMenu97::~cMenu97() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu97::mGoToChild()
{
	if(actual_line == 1) return new cMenu971(config,this);
	if(actual_line == 2) return new cMenu972(config,this);
	if(actual_line == 3) return new cMenu973(config,this);

	return NULL;
}

