/*
 * cMenu927.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */


#include "../../inc/menu/cMenu927.hpp"


cMenu927::cMenu927(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "__9.2.7 Idt do Rmtt_";
	menu_art[1] = "1. Ligado           ";
	menu_art[2] = "2. Desligado        ";


	mPrintMenu();

}

cMenu927::~cMenu927() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu927::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();


	return NULL;

}

cMenu * cMenu927::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu927::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

