/*
 * cMenu96.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu96.hpp"

cMenu96::cMenu96(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "__9.6 Monitoramento_";
	menu_art[1] = "1. Liga/Desliga     ";
	menu_art[2] = "2. Porta UDP        ";

	mPrintMenu();

}

cMenu96::~cMenu96() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu96::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();


	return NULL;
}

cMenu * cMenu96::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu96::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

