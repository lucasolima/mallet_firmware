/*
 * cMenu973.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */


#include "../../inc/menu/cMenu973.hpp"


cMenu973::cMenu973(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "__9.7.3 Roteamento__";
	menu_art[1] = "1. Rota Padrao      ";
	menu_art[2] = "2. Rotas            ";

	mPrintMenu();

}

cMenu973::~cMenu973() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu973::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;

}

cMenu * cMenu973::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu973::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
