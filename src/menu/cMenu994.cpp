/*
 * cMenu994.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu994.hpp"


cMenu994::cMenu994(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "__9.9.4 Hora Remota_";
	menu_art[1] = "1. Aceito           ";
	menu_art[2] = "2. Nao Aceito       ";

	mPrintMenu();

}

cMenu994::~cMenu994() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu994::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;

}

cMenu * cMenu994::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu994::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
