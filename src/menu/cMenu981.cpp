/*
 * cMenu981.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */
#include "../../inc/menu/cMenu981.hpp"


cMenu981::cMenu981(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub

	if(config.Root["Settings"]["IsVeicular"].asBool())
	{
		length = 4;
		menu_art = new string[length+1];

		menu_art[0] = "___9.8.1 Potencia___";
		menu_art[1] = "1. 1W               ";
		menu_art[2] = "3. 10W              ";
		menu_art[3] = "4. 25W              ";
		menu_art[4] = "5. 50W              ";
	}
	else if(!config.Root["Settings"]["IsVeicular"].asBool())
	{
		length = 4;
		menu_art = new string[length+1];

		menu_art[0] = "___9.8.1 Potencia___";
		menu_art[1] = "1. 1W               ";
		menu_art[2] = "2. 2W               ";
		menu_art[3] = "3. 5W               ";
		menu_art[4] = "4. 10W              ";

	}

	mPrintMenu();

}

cMenu981::~cMenu981() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu981::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();
	if(actual_line == 4) return Function4();

	return NULL;

}

cMenu * cMenu981::Function1()
{
	//implementar aqui
	config.Root["Settings"]["PowerAmplifier"] = Low;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo    ");
	mPrintMenu();
	return this;
}

cMenu * cMenu981::Function2()
{
	//implementar aqui
	config.Root["Settings"]["PowerAmplifier"] = MediumLow;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo    ");
	mPrintMenu();
	return this;
}

cMenu * cMenu981::Function3()
{
	//implementar aqui
	config.Root["Settings"]["PowerAmplifier"] = MediumHigh;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo    ");
	mPrintMenu();
	return this;
}

cMenu * cMenu981::Function4()
{
	config.Root["Settings"]["PowerAmplifier"] = High;
	config.UpdateFile();
	mPrintMessage("    Valor Salvo    ");
	mPrintMenu();
	return this;
}





