/*
 * cMenu933.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu933.hpp"


cMenu933::cMenu933(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 2;

	menu_art = new string[length+1];

	menu_art[0] = "_9.3.3 Sincronizacao";
	menu_art[1] = "1. Varredura        ";
	menu_art[2] = "2. Relogio          ";

	mPrintMenu();

}

cMenu933::~cMenu933() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu933::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();

	return NULL;

}

cMenu * cMenu933::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu933::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

