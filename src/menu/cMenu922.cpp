/*
 * cMenu922.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */
#include "../../inc/menu/cMenu922.hpp"


cMenu922::cMenu922(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 4;

	menu_art = new string[length+1];

	menu_art[0] = "____9.2.2 Vocoder___";
	menu_art[1] = "1. TWELP            ";
	menu_art[2] = "2. CVSD             ";
	menu_art[3] = "3. LPCM             ";
	menu_art[4] = "4. G729A            ";

	mPrintMenu();

}

cMenu922::~cMenu922() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu922::mGoToChild()
{
	if(actual_line == 1)
	{
		char buf[6];
		buf[0] = CHANGE_MODCOD;
		buf[1] = (FSK4 & 0xFF00) >> 8;
		buf[2] = FSK4 & 0x00FF;
		buf[3] = TWELP;
		buf[4] = PTT_OFF;
		buf[5] = 0;
		int fdmain = open("/dev/RxTx_Controller",O_RDWR);
		int ret = write(fdmain,buf,4);
		ret = write(fdmain,&buf[4],2);
		mPrintMessage("     Valor Salvo    ");
		return mGoToParent();
	}
	else if(actual_line == 2)
	{
		char buf[6];
		buf[0] = CHANGE_MODCOD;
		buf[1] = (FSK4 & 0xFF00) >> 8;
		buf[2] = FSK4 & 0x00FF;
		buf[3] = CVSD;
		buf[4] = PTT_OFF;
		buf[5] = 0;
		int fdmain = open("/dev/RxTx_Controller",O_RDWR);
		int ret = write(fdmain,buf,4);
		ret = write(fdmain,&buf[4],2);
		mPrintMessage("     Valor Salvo    ");
		return mGoToParent();
	}
	else if(actual_line == 3)
	{
		char buf[6];
		buf[0] = CHANGE_MODCOD;
		buf[1] = (FSK4 & 0xFF00) >> 8;
		buf[2] = FSK4 & 0x00FF;
		buf[3] = LPCM;
		buf[4] = PTT_OFF;
		buf[5] = 0;
		int fdmain = open("/dev/RxTx_Controller",O_RDWR);
		int ret = write(fdmain,buf,4);
		ret = write(fdmain,&buf[4],2);
		mPrintMessage("     Valor Salvo    ");
		return mGoToParent();
	}
	else if(actual_line == 4)
	{
		char buf[6];
		buf[0] = CHANGE_MODCOD;
		buf[1] = (FSK4 & 0xFF00) >> 8;
		buf[2] = FSK4 & 0x00FF;
		buf[3] = G729A;
		buf[4] = PTT_OFF;
		buf[5] = 0;
		int fdmain = open("/dev/RxTx_Controller",O_RDWR);
		int ret = write(fdmain,buf,4);
		ret = write(fdmain,&buf[4],2);
		mPrintMessage("     Valor Salvo    ");
		return mGoToParent();
	}
	return NULL;

}

