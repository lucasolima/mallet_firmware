/*
 * cMenu971.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu971.hpp"


cMenu971::cMenu971(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 3;

	menu_art = new string[length+1];

	menu_art[0] = "___9.7.1 Ethernet___";
	menu_art[1] = "1. Endereco IP      ";
	menu_art[2] = "2. Msc de Rede      ";
	menu_art[3] = "3. End Maquina      ";

	mPrintMenu();

}

cMenu971::~cMenu971() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu971::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return Function2();
	if(actual_line == 3) return Function3();

	return NULL;

}

cMenu * cMenu971::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu971::Function2()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

cMenu * cMenu971::Function3()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}
