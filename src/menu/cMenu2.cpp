/*
 * cMenu2.cpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#include "../../inc/menu/cMenu2.hpp"

cMenu2::cMenu2(cConfigurations& oConfigurations, cMenu* prnt) : cMenu(oConfigurations, prnt)  {
	// TODO Auto-generated constructor stub
	length = 4;

	menu_art = new string[length+1];

	menu_art[0] = "_____2. Canais______";
	menu_art[1] = "1. Operacao         ";
	menu_art[2] = "2. Pre-Programados  ";
	menu_art[3] = "3. Modo             ";
	menu_art[4] = "4. Tx 150Hz         ";



	mPrintMenu();
}

cMenu2::~cMenu2() {
	// TODO Auto-generated destructor stub
	delete menu_art;
}

cMenu * cMenu2::mGoToChild()
{
	if(actual_line == 1) return Function1();
	if(actual_line == 2) return new cMenu22(config,this);
	if(actual_line == 3) return new cMenu23(config,this);
	if(actual_line == 4) return Function4();

	return NULL;
}

cMenu * cMenu2::Function1()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}


cMenu * cMenu2::Function4()
{
	//implementar aqui
	mPrintMessage("To Be Implemented...");
	mPrintMenu();
	return this;
}

