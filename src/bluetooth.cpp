
#include "../inc/bluetooth.hpp"

cBluetooth::cBluetooth(){
	//
}

cBluetooth::~cBluetooth() {
	//
}

/**
 * turns the local bluetooth device on
 */
void cBluetooth::mTurnOn(){

	fstream hcidConf;
	hcidConf.open(HCID_CONFIG_FILE, ios::in);
	fstream hciTmp;
	hciTmp.open(HCI_TMP_FILE, ios::out);

	std::string line;
	int count = 0;
	while (getline( hcidConf, line )){
		count++;
			if (count == 4){ // line 4 with hci0 autoinit
				hciTmp << "\tautoinit yes;" << endl;
			}else{
				hciTmp << line << endl;
			}
	}
	hcidConf.close();
	hciTmp.close();
	remove(HCID_CONFIG_FILE);
	rename(HCI_TMP_FILE, HCID_CONFIG_FILE);

	system("hciconfig hci0 up > /var/log/bluetooth.log");
}

/**
 * turns the local bluetooth device off
 */
void cBluetooth::mTurnOff(){

	fstream hciConf;
	hciConf.open(HCID_CONFIG_FILE, ios::in);
	fstream hciTmp;
	hciTmp.open(HCI_TMP_FILE, ios::out);

	std::string line;
	int count = 0;
	while (getline( hciConf, line )){
		count++;
			if (count == 4){ // line 4 with hci0 autoinit
				hciTmp << "\tautoinit no;" << endl;
			}else{
				hciTmp << line << endl;
			}
	}
	hciConf.close();
	hciTmp.close();
	remove(HCID_CONFIG_FILE);
	rename(HCI_TMP_FILE, HCID_CONFIG_FILE);

	system("hciconfig hci0 down > /var/log/bluetooth.log");
}

/**
 * changes the name of the local bluetooth device
 * @param name new name of the device
 */
void cBluetooth::mChangeName(const char* name){

	fstream hciConf;
	hciConf.open(HCID_CONFIG_FILE, ios::in);
	fstream hciTmp;
	hciTmp.open(HCI_TMP_FILE, ios::out);

	std::string line;
	int count = 0;
	while (getline( hciConf, line )){
		count++;
			if (count == 11){ // line 11 with hci0 name
				hciTmp << "\tname \"" << name << "\";"<< endl;
			}else{
				hciTmp << line << endl;
			}
	}
	hciConf.close();
	hciTmp.close();
	remove(HCID_CONFIG_FILE);
	rename(HCI_TMP_FILE, HCID_CONFIG_FILE);

	char cmd[70];
	strcpy(cmd,"hciconfig hci0 name '");
	strcat(cmd, name);
	strcat(cmd,"' > /var/log/bluetooth.log");

	system(cmd);
}

/**
 * changes the pin in the files and reloads the HCI Daemon.
 * @param pin personal identification number
 */
void cBluetooth::mChangePin(const char* pin){

	//replace pin helper file
	remove(PIN_HELPER_FILE);
	ofstream pinHelper(PIN_HELPER_FILE);
	//pinHelper.open(PIN_HELPER_FILE, ios::);
	pinHelper << "#!/bin/sh" << endl;
	pinHelper << "echo \"PIN:" << pin << "\"\n" << endl;
	pinHelper.close();
	system("chmod 777 /usr/bin/bluepin");

	//replace pin file
	remove(PIN_FILE);
	ofstream pinFile(PIN_FILE);
	//pinFile.open(PIN_FILE, ios::in);
	pinFile << pin << endl;
	pinFile.close();

	//reset HCI Daemon and its Security Manager
	system("killall hcid  > /var/log/bluetooth.log");
	system("hcid -n &  > /var/log/bluetooth.log");
}

/**
 * runs 'hcitool scan' to scan for bluetooth devices and list them in the returned value
 * @return vector of bluetooth devices
 */
std::vector<sBlueDev> cBluetooth::mScan(){

	system("hcitool scan  > /var/log/bluetooth.log");

	std::vector<sBlueDev> devs;
	fstream scannedDevs;
	scannedDevs.open("/var/log/bluetooth.log", ios::in);

	std::string line;
	int count = 0;
	while (getline( scannedDevs, line )){
		count++;
			if (count > 1){
				sBlueDev newDev;
				newDev.baddr = line.substr(1,17);
				newDev.name = line.substr(19,20);
				devs.push_back(newDev);
			}
	}

	return devs;
}

/**
 * sends request for pairing to the bluetooth device
 * @param dev bluetooth device
 * @return true if link key was established. false otherwise.
 */
bool cBluetooth::mPair(sBlueDev dev){

	std::string cmd = "hcitool cc " + dev.baddr + " 2> /var/log/bluetooth.log";
	system(cmd.c_str());

	fstream logfile;
	logfile.open("/var/log/bluetooth.log", ios::in);

	std::string err = "Can't create connection:";
	std::string line;
	getline( logfile, line );

	return std::string::npos == line.find(err);
}

/**
 * runs the pand command to connect the bluetooth device to the network
 * @param dev bluetooth device
 * @return true if connection could create the network interface. false otherwise.
 */
bool cBluetooth::mConnect(sBlueDev dev){

	//clear log
	system("echo '' > /var/log/bluetooth.log");
	// command to connect
	std::string cmd = "pand -c " + dev.baddr;
	system(cmd.c_str());

	fstream logfile;
	std::string line;
	// check connection
	// the file /etc/bluetooth/pan/dev-up must have the commmand echo 'interface created' > /var/log/bluetooth.log

	usleep(1000000);

	logfile.open("/var/log/bluetooth.log", ios::in);

	//getline( logfile, line );

	getline( logfile, line );
	fprintf(stderr,"%s     %d   \n",      line.c_str(),     (std::string::npos != line.find("interface created"))     );


	//fprintf(stderr,"%s     %d   \n",      line.c_str(),     (std::string::npos != line.find("interface created"))     );

	return std::string::npos != line.find("interface created");
}

void cBluetooth::mInit(char *name, char* pin, bool state)
{
	fstream hciConf;
	hciConf.open(HCID_CONFIG_FILE, ios::in);
	fstream hciTmp;
	hciTmp.open(HCI_TMP_FILE, ios::out);

	std::string line;
	int count = 0;
	while (getline( hciConf, line )){
		count++;
			if (count == 11 || count == 4){
				if(count == 11) hciTmp << "\tname \"" << name << "\";"<< endl;
				if(count == 4){
					if(state){
						hciTmp << "\tautoinit yes;" << endl;
					}else{
						hciTmp << "\tautoinit no;" << endl;
					}
				}
			}else{
				hciTmp << line << endl;
			}
	}
	hciConf.close();
	hciTmp.close();
	remove(HCID_CONFIG_FILE);
	rename(HCI_TMP_FILE, HCID_CONFIG_FILE);

	//replace pin helper file
	remove(PIN_HELPER_FILE);
	ofstream pinHelper(PIN_HELPER_FILE);
	//pinHelper.open(PIN_HELPER_FILE, ios::);
	pinHelper << "#!/bin/sh" << endl;
	pinHelper << "echo \"PIN:" << pin << "\"\n" << endl;
	pinHelper.close();
	system("chmod 777 /usr/bin/bluepin");

	//replace pin file
	remove(PIN_FILE);
	ofstream pinFile(PIN_FILE);
	//pinFile.open(PIN_FILE, ios::in);
	pinFile << pin << endl;
	pinFile.close();

}

