/*
 * Modem_Commands.h
 *
 *  Created on: May 31, 2016
 *      Author: lucasolim
 */

#ifndef MODEM_COMMANDS_H_
#define MODEM_COMMANDS_H_

#include "inc/cConfigurations.hpp"


/**
 * Mensagem de Texto
 * Comando seguido de uma estrutura serializada, deve ser mandada sem se preocupar com o que tem dentro.
 */
#define MODEM_SMS_CMD 0x00
#define SIZEOF_MODEM_SMS_CMD (sizeof(sMessage) + 1)


/**
 * Mudança de frequencia, comando seguido de 2 Bytes que definem frequencia.
 * Frequencia Zero define modo SEM multiplexação de tempo,
 * Frequencias 1,2,3,4 definem Multiplex de Tempo
 * Comando de 3 Bytes
 *
 *     __  __  __
 *    Cmd  Fq  Fq
 *
 *
 */
#define MODEM_SET_FREQ_0_RX 0x05
#define SIZEOF_MODEM_SET_FREQ_0_RX 3

#define MODEM_SET_FREQ_1_RX 0x01
#define SIZEOF_MODEM_SET_FREQ_1_RX 3

#define MODEM_SET_FREQ_2_RX 0x02
#define SIZEOF_MODEM_SET_FREQ_2_RX 3

#define MODEM_SET_FREQ_3_RX 0x03
#define SIZEOF_MODEM_SET_FREQ_3_RX 3

#define MODEM_SET_FREQ_4_RX 0x04
#define SIZEOF_MODEM_SET_FREQ_4_RX 3

#define MODEM_SET_FREQ_0_TX 0x15
#define SIZEOF_MODEM_SET_FREQ_0_TX 3

#define MODEM_SET_FREQ_1_TX 0x11
#define SIZEOF_MODEM_SET_FREQ_1_TX 3

#define MODEM_SET_FREQ_2_TX 0x12
#define SIZEOF_MODEM_SET_FREQ_2_TX 3

#define MODEM_SET_FREQ_3_TX 0x13
#define SIZEOF_MODEM_SET_FREQ_3_TX 3

#define MODEM_SET_FREQ_4_TX 0x14
#define SIZEOF_MODEM_SET_FREQ_4_TX 3

/**
 * PTT 1~4
 */

#define PTT0_ON  0x06
#define PTT0_OFF 0x07

#define PTT1_ON  0x08
#define PTT1_OFF 0x09

#define PTT2_ON  0x0A
#define PTT2_OFF 0x0B

#define PTT3_ON  0x0C
#define PTT3_OFF 0x0D

/**
 * Codificações
 */

#define COD_TWELPE 		0x0E
#define COD_LPCM   		0x0F
#define COD_CVSD   		0x10
#define COD_6729A  		0x16
#define COD_6711A  		0x17
#define COD_6711XOR 	0x18
#define COD_6711U		0x19
#define COD_6711XOR		0x1A

/**
 * Interfaces de Rede / Frequencia -> Tx -> Rx
 * Comando de 6 Bytes
 *    __   __   __   __   __   __
 *   Cmd   Rx   Rx   Tx   Tx   TS
 *
 *   TS = 0 para modo não multiplexado no tempo
 *   TS = 1~4 para multiplexação em tempo
 */

#define ETH1_FREQ		0x20
#define ETH2_FREQ		0x21
#define ETH3_FREQ		0x22
#define ETH4_FREQ		0x23
#define ETH5_FREQ		0x24
#define ETH6_FREQ		0x25
#define ETH7_FREQ		0x26
#define ETH8_FREQ		0x27
#define ETH9_FREQ		0x28
#define ETH10_FREQ		0x29
#define ETH11_FREQ		0x2A
#define ETH12_FREQ		0x2B
#define ETH13_FREQ		0x2C
#define ETH14_FREQ		0x2D
#define ETH15_FREQ		0x2E
#define ETH16_FREQ		0x2F

#define SIZEOF_ETH1_FREQ		6
#define SIZEOF_ETH2_FREQ		6
#define SIZEOF_ETH3_FREQ		6
#define SIZEOF_ETH4_FREQ		6
#define SIZEOF_ETH5_FREQ		6
#define SIZEOF_ETH6_FREQ		6
#define SIZEOF_ETH7_FREQ		6
#define SIZEOF_ETH8_FREQ		6
#define SIZEOF_ETH9_FREQ		6
#define SIZEOF_ETH10_FREQ		6
#define SIZEOF_ETH11_FREQ		6
#define SIZEOF_ETH12_FREQ		6
#define SIZEOF_ETH13_FREQ		6
#define SIZEOF_ETH14_FREQ		6
#define SIZEOF_ETH15_FREQ		6
#define SIZEOF_ETH16_FREQ		6



#endif /* MODEM_COMMANDS_H_ */
