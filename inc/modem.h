/*
 * modem.h
 *
 *  Created on: Jun 6, 2016
 *      Author: eder
 */

#ifndef MODEM_H_
#define MODEM_H_

#include <sys/types.h>

typedef __u8 u8;
typedef __u16 u16;


#include "codec.h"

#define MODULATION_FSK2				((u16)0x0202)
#define MODULATION_FSK4				((u16)0x0204)
#define MODULATION_FSK8				((u16)0x0208)
#define MODULATION_FSK16			((u16)0x0210)
#define MODULATION_4QAM				((u16)0x0404)
#define MODULATION_16QAM			((u16)0x0410)
#define MODULATION_32QAM			((u16)0x0420)
#define MODULATION_64QAM			((u16)0x0440)
#define MODULATION_GMSK_GFSK		((u16)0x0100)
typedef enum
{
	FSK2 			=  MODULATION_FSK2,
	FSK4			=  MODULATION_FSK4,
	FSK8			=  MODULATION_FSK8,
	FSK16			=  MODULATION_FSK16,
	QAM4			=  MODULATION_4QAM,
	QAM16			=  MODULATION_16QAM,
	QAM32			=  MODULATION_32QAM,
	QAM64			=  MODULATION_64QAM,
	GMSK_GFSK		=  MODULATION_GMSK_GFSK,
	NO_MODULATION   = ((u16)0),
	FM				=  ((u16)0x0300)
}Modulation_Type_TypeDef;
//TODO: For future use, its possible to add more Tx and Rx modes (FSK2, FSK3, FSK4)
typedef enum
{
	FM_TX = (u16)(0x0000),
	FM_RX = (u16)(0x0001),
	FM_OFF = (u16)(0x0002),
	NO_MODE = (u16)0x0003
}FmTrc_Mode_Typedef;
//TODO: Implement other BAUDRATES for diferent images.
//#define BAUDRATE_10000 			((u16)10000)
#define BAUDRATE_9600 				((u16)9600)
#define BAUDRATE_4800 				((u16)4800)
#define MODEM_MTU				(u16)(255)
#define MODEM_BLOCK_SIZE		(u16)(15) //number of bytes per block of modem raw transfer max 15
#define HEADER_LEN				MODEM_BLOCK_SIZE

#define MODEM_IRQ_PORT	0x42	//PE2 - user button on stm32f4som
//TODO: replace packet types below by a typedef enum Packet_TypeDef
#define AUDIO_PACKET  ((u8)0x06)
//#define AUDIO_PACKET_CVSD	((u8)0x05)
//#define AUDIO_PACKET_G711A	((u8)0x04)
//#define AUDIO_PACKET_G729A	((u8)0x03)
//#define AUDIO_PACKET_TWELP	((u8)0x07)
#define ETHER_PACKET  ((u8)0x01)
#define INVALID_PACKET ((u8)0xFF)
struct frame
{
	u16 frame_len;
	u8 frame_type;
	u8 count;
	Codification_TypeDef audio_cod;
	Rate_TypeDef	audio_rate;
	u8 frame_header[HEADER_LEN];
	u8 frame_data[MODEM_MTU];
};
/**
 * Mensagem de Texto
 * Comando seguido de uma estrutura serializada, deve ser mandada sem se preocupar com o que tem dentro.
 */
#define MODEM_SMS_CMD 0x00
#define SIZEOF_MODEM_SMS_CMD (sizeof(sMessage) + 1)

#define MAX_CHANNEL_NUMBER		4
/**
 * Mudança de frequencia, comando seguido de 2 Bytes que definem frequencia.
 * Frequencia Zero define modo SEM multiplexação de tempo,
 * Frequencias 1,2,3,4 definem Multiplex de Tempo
 * Comando de 3 Bytes
 *
 *     __ ____ __  __
 *    Cmd  no  Fq  Fq
 *
 *
 */
#define MODEM_SET_FREQ_RX 0x05
#define SIZEOF_MODEM_SET_FREQ_RX 4

#define MODEM_SET_FREQ_TX 0x15
#define SIZEOF_MODEM_SET_FREQ_TX 4

/**
 * PTT 1~4
 * ____   _____
 * PTT    PTTno
 */

#define PTT_ON  0x06
#define PTT_OFF 0x07


//TODO: Poderia colocar comandos para trocar as taxas de amostragem do áudio. Implementar na versão 2.0.

/**
 * Interfaces de Rede / Frequencia -> Tx -> Rx
 * Comando de 6 Bytes
 *    __  _____  __   __   __   __   __
 *   Cmd  Ethno	 Rx   Rx   Tx   Tx   TS
 *
 *   TS = 0 para modo não multiplexado no tempo
 *   TS = 1~4 para multiplexação em tempo
 */
#define MAX_ETH_NUMBER	 16
#define ETH_FREQ		0x20
#define SIZEOF_ETH_FREQ		7

/**
 * Troca de Modulação/Codificação
 *
 *  ___	 ____  ____   ___
 *  cmd	  mod   mod	  cod
 *  mod é um código de 16 bits do tipo Modulation_Type_TypeDef e cod é a codificação 8bits do tipo Codification_TypeDef
 */
#define CHANGE_MODCOD	0x30

#endif /* MODEM_H_ */
