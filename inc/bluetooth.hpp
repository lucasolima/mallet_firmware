
#ifndef INC_BLUETOOTH_HPP_
#define INC_BLUETOOTH_HPP_

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <stdlib.h>
#include <unistd.h>


#define HCID_CONFIG_FILE	"/etc/bluetooth/hcid.conf"
#define PIN_FILE			"/etc/bluetooth/pin"
#define PIN_HELPER_FILE		"/usr/bin/bluepin"
#define HCI_TMP_FILE		"/etc/bluetooth/hcidtmp"

typedef struct {
	std::string name;
	std::string baddr;
}sBlueDev;

using namespace std;

class cBluetooth{
public:
	cBluetooth();
	virtual ~cBluetooth();
	void mTurnOn();
	void mTurnOff();
	void mChangeName(const char* name);
	void mChangePin(const char* pin);
	void mInit(char *name, char* pin, bool state);
	std::vector<sBlueDev> mScan();
	bool mPair(sBlueDev dev);
	bool mConnect(sBlueDev dev);
};

#endif /* INC_BLUETOOTH_HPP_ */
