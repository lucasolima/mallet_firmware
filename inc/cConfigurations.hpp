/*
 * cConfigurations.hpp
 *
 *  Created on: May 13, 2016
 *      Author: lucasolim
 */

#ifndef INC_CCONFIGURATIONS_HPP_
#define INC_CCONFIGURATIONS_HPP_

#include <linux/types.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include "bluetooth.hpp"
#include "json/json.h"
#include "modem.h"
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>

using namespace std;

typedef enum{AudioSpkVol, AudioMicVol, IsVeicular, BatteryLevel, Rssi} HwParam;

typedef enum{Disable = 0, Enable  = 1} Status_t;

typedef enum{Low = 0 , MediumLow = 1, MediumHigh = 2, High = 3} Power_t;

typedef enum{Raw = 0} GpsType_t;

typedef enum{None = 0, AES64 = 1, AES128 = 2, AES192 = 3, AES256 = 4} Crypto_t;



class cConfigurations {
public:
	cConfigurations(string ConfigurationFile);
	virtual ~cConfigurations();
	int UpdateFile();
	cBluetooth Blue;
	Json::Value Root;
	int GetIntHwParam(HwParam p);
	int UpdateChannel();
	int UpdateBluetooth();
	int UpdateEthernet();
	int UpdateTdmChannel();
	int UpdateTdmStatus(bool status);
	void PttOn();
	void PttOff();

private:
	string BD_Path;
};

#endif /* INC_CCONFIGURATIONS_HPP_ */
