/*
 * codec.h
 *
 *  Created on: May 31, 2016
 *      Author: eder
 */
#ifndef CODEC_H_
#define CODEC_H_
#define OUTPUT_DATA_AVAILABLE 	1
#define INPUT_DATA_WANTED 		2
typedef enum{
	ANAIN1 = ((u16)0x0000),
	ANAIN2 = ((u16)0x0001),
	CBUS = ((u16)0x0002), //dont change this
	EXT_PCM = ((u16)0x0004),//dont change this
	/** Analog destinations - First word is the DAC used, second word is the analog destination. Only implemented speaker outputs */
	ANAOUT_SPK1 = ((u16)0x0020), //Ex: Using DAC ANAOUT with speaker 1 output
	ANAOUT_SPK2 = ((u16)0x0040),
	ANAOUT_SPK12 = ((u16)0x0080),
	MONOUT_SPK1 = ((u16)0x0100),
	MONOUT_SPK2 = ((u16)0x0200),
	MONOUT_SPK12 = ((u16)0x0400),
}SrcDest_TypeDef;
typedef enum{
	CVSD 	= ((u8)0x13),
	G729A 	= ((u8)0x14),
	G711A 	= ((u8)0x15),
	G711A_XOR= ((u8)0x16),
	G711U 	= ((u8)0x17),
	G711U_XOR=((u8)0x18),
	LPCM 	= ((u8)0x19),
	TWELP 	= ((u8)0x20),
	INVALID_COD = ((u8)0x21)
}Codification_TypeDef;
typedef enum{
	RATE_32KHZ = ((u8)0x22), //CVSD only
	RATE_16KHZ = ((u8)0x23),
	RATE_8KHZ = ((u8)0x24),
	INVALID_RATE = ((u8)0x25)
}Rate_TypeDef;
#endif /* CODEC_H_ */
