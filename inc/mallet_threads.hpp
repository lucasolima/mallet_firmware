/*
 * mallet_threads.hpp
 *
 *  Created on: May 13, 2016
 *      Author: lucasolim
 */

#ifndef INC_MALLET_THREADS_HPP_
#define INC_MALLET_THREADS_HPP_

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <iostream>
#include<linux/input.h>
#include "menu/cMenu.hpp"
#include "menu/cMainMenu.hpp"


extern bool FlagSigint ;
extern void sigint_handle(int sig);



extern pthread_t thread_keyscreen;
extern void *Thread_KeyScreen(void *ptr);
extern bool flag_keyboard ;
extern int ret_keyboard ;







#endif /* INC_MALLET_THREADS_HPP_ */
