/*
 * cMenu.hpp
 *
 *  Created on: May 13, 2016
 *      Author: lucasolim
 */

#include "../cConfigurations.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <vector>
#include <fstream>
#include "../gps/minmea.h"
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "../bluetooth.hpp"
#include <string.h>
#include "../modem.h"

#ifndef INC_CMENU_HPP_
#define INC_CMENU_HPP_

using namespace std;


typedef struct{
	char keys[7];
	int length;
	int ptr;
}sTextKey;



class cMenu {
public:
	cMenu(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu();
	int mGetActualLine();
	int mGetLength();
	int mGetInitLine();
	int mGetLineWithArrow();
	cMenu* mGetParent();
	cMenu* mGoToParent();
	virtual cMenu * mGoToChild();
	virtual int mScrollDown(int n);
	virtual int mScrollUp(int n);
	virtual int mJumpToLine(int n);
	friend ostream& operator << (ostream& os, const cMenu& menu);
	virtual void mPrintMenu();
	virtual void mUpdateTime();
	void mPrintMessage(string s);
	void mPrintMessageNoDelay(string s);
	virtual void mUpdate();
	virtual cMenu * mErase();
	bool function_is_running_flag;
	char function_key_pressed;
	bool function_flag_timeout; //avisa um timeout de teclado, útil para funções que esperam mais de uma paertada no teclado para gerar um caracter
	bool function_text; // avisa que a função em execução aguarda texto como entrada

protected:
	cMenu* parent;
	int length; //numero de linhas
	int actual_line; //linha ativa no display
	int init_line; // primeira linha no display
	int line_with_arrow; // linha aponrtada no display
	string* menu_art;
	int* function_pointer;
	int function_pointer_counter;
	int function_count;
	bool function_flag_keep_running;
	int mGetText(char* buf,int buf_actual_size ,int buf_size, char* function_name);
	int mGetIP(char* buf, char* function_name);
	bool IP_Mask[15];
	sTextKey KeyboarMap[10];
	cConfigurations& config;



};



#endif /* INC_CMENU_HPP_ */
