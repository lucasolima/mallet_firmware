/*
 * cMenu93.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU93_HPP_
#define INC_MENU_CMENU93_HPP_

#include "cMenu.hpp"
#include "cMenu933.hpp"

class cMenu93 : public cMenu {
public:
	cMenu93(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu93();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function4();
	cMenu * Function5();
};

#endif /* INC_MENU_CMENU93_HPP_ */
