/*
 * Menu911.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU911_HPP_
#define INC_MENU_CMENU911_HPP_

#include "cMenu.hpp"

class cMenu911 : public cMenu{
public:
	cMenu911(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu911();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();

};

#endif /* INC_MENU_CMENU911_HPP_ */
