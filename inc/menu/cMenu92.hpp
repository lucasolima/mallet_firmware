/*
 * cMenu92.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU92_HPP_
#define INC_MENU_CMENU92_HPP_

#include "cMenu.hpp"
#include "cMenu921.hpp"
#include "cMenu922.hpp"
#include "cMenu926.hpp"
#include "cMenu927.hpp"

class cMenu92 : public cMenu {
public:
	cMenu92(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu92();
	cMenu * mGoToChild();
private:

};

#endif /* INC_MENU_CMENU92_HPP_ */
