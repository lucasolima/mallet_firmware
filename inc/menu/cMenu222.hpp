/*
 * Menu222.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU222_HPP_
#define INC_MENU_CMENU222_HPP_

#include "cMenu.hpp"
#include "cMenuList.hpp"

class cMenu222 : public cMenu{
public:
	cMenu222(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu222();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
	cMenu * Function4();
	int *ret;

};

#endif /* INC_MENU_CMENU222_HPP_ */
