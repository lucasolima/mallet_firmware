/*
 * cMenu71.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU71_HPP_
#define INC_MENU_CMENU71_HPP_

#include "cMenu.hpp"

class cMenu71 : public cMenu {
public:
	cMenu71(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu71();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
};

#endif /* INC_MENU_CMENU71_HPP_ */
