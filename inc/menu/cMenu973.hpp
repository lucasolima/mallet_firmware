/*
 * cMenu973.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU973_HPP_
#define INC_MENU_CMENU973_HPP_

#include "cMenu.hpp"

class cMenu973: public cMenu {
public:
	cMenu973(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu973();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();

};

#endif /* INC_MENU_CMENU973_HPP_ */
