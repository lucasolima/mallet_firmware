/*
 * cMenu94.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU94_HPP_
#define INC_MENU_CMENU94_HPP_

#include "cMenu.hpp"

class cMenu94 : public cMenu {
public:
	cMenu94(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu94();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();

};

#endif /* INC_MENU_CMENU94_HPP_ */
