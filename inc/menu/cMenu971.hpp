/*
 * cMenu971.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU971_HPP_
#define INC_MENU_CMENU971_HPP_

#include "cMenu.hpp"

class cMenu971 : public cMenu{
public:
	cMenu971(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu971();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
};

#endif /* INC_MENU_CMENU971_HPP_ */
