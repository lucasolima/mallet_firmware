/*
 * cMenu72.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU72_HPP_
#define INC_MENU_CMENU72_HPP_

#include "cMenu.hpp"

class cMenu72 : public cMenu{
public:
	cMenu72(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu72();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
};

#endif /* INC_MENU_CMENU72_HPP_ */
