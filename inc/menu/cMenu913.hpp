/*
 * cMenu913.h
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU913_HPP_
#define INC_MENU_CMENU913_HPP_

#include "cMenu.hpp"

class cMenu913 : public cMenu {
public:
	cMenu913(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu913();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
};

#endif /* INC_MENU_CMENU913_HPP_ */
