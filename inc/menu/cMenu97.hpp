/*
 * cMenu97.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU97_HPP_
#define INC_MENU_CMENU97_HPP_

#include "cMenu.hpp"
#include "cMenu971.hpp"
#include "cMenu972.hpp"
#include "cMenu973.hpp"

class cMenu97 : public cMenu{
public:
	cMenu97(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu97();
	cMenu * mGoToChild();

};

#endif /* INC_MENU_CMENU97_HPP_ */
