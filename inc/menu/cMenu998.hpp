/*
 * cMenu998.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU998_HPP_
#define INC_MENU_CMENU998_HPP_

#include "cMenu.hpp"

class cMenu998: public cMenu {
public:
	cMenu998(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu998();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
	cMenu * Function4();
};

#endif /* INC_MENU_CMENU998_HPP_ */
