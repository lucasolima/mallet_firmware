/*
 * cMenu9.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU9_HPP_
#define INC_MENU_CMENU9_HPP_

#include "cMenu.hpp"
#include "cMenu91.hpp"
#include "cMenu92.hpp"
#include "cMenu93.hpp"
#include "cMenu94.hpp"
#include "cMenu95.hpp"
#include "cMenu96.hpp"
#include "cMenu97.hpp"
#include "cMenu98.hpp"
#include "cMenu99.hpp"

class cMenu9 : public cMenu {
public:
	cMenu9(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu9();
	cMenu * mGoToChild();
};

#endif /* INC_MENU_CMENU9_HPP_ */
