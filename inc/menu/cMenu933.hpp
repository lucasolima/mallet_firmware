/*
 * cMenu933.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU933_HPP_
#define INC_MENU_CMENU933_HPP_

#include "cMenu.hpp"

class cMenu933 : public cMenu {
public:
	cMenu933(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu933();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();

};

#endif /* INC_MENU_CMENU933_HPP_ */
