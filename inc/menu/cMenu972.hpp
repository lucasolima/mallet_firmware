/*
 * cMenu972.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU972_HPP_
#define INC_MENU_CMENU972_HPP_

#include "cMenu.hpp"

class cMenu972 : public cMenu{
public:
	cMenu972(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu972();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();

};

#endif /* INC_MENU_CMENU972_HPP_ */
