/*
 * cMenu98.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU98_HPP_
#define INC_MENU_CMENU98_HPP_

#include "cMenu.hpp"
#include "cMenu981.hpp"
#include "cMenu982.hpp"

class cMenu98 : public cMenu {
public:
	cMenu98(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu98();
	cMenu * mGoToChild();

};

#endif /* INC_MENU_CMENU98_HPP_ */
