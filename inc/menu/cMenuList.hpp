/*
 * cMenuList.hpp
 *
 *  Created on: May 23, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENULIST_HPP_
#define INC_MENU_CMENULIST_HPP_

#include "cMenu.hpp"

class cMenuList: public cMenu {
public:
	cMenuList(cConfigurations& oConfigurations, cMenu* prnt, string* list_art, int list_length, Json::Value& parameter);
	virtual ~cMenuList();
	cMenu* mGoToChild();
private:
	Json::Value& parameter_to_be_changed;
};

#endif /* INC_MENU_CMENULIST_HPP_ */
