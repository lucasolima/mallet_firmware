/*
 * cMenuText.hpp
 *
 *  Created on: May 25, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENUTEXT_HPP_
#define INC_MENU_CMENUTEXT_HPP_

#include "cMenu.hpp"
#include "cMenu1.hpp"
#include "../json/json.h"

class cMenuText : public cMenu {
public:
	cMenuText(cConfigurations& oConfigurations, cMenu* prnt, string* list_art, Json::Value& BoxMsg, const char* path);
	virtual ~cMenuText();
	cMenu* mGoToChild();
	virtual cMenu* mErase();
private:
	Json::Value& MsgBuf;
	const char* Path;
	//int  counter;
};

#endif /* INC_MENU_CMENUTEXT_HPP_ */
