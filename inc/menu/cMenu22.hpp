/*
 * cMenu22.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU22_HPP_
#define INC_MENU_CMENU22_HPP_

#include "cMenu.hpp"
#include "cMenu222.hpp"

class cMenu22 : public cMenu{
public:
	cMenu22(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu22();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
	cMenu * Function4();
};

#endif /* INC_MENU_CMENU22_HPP_ */
