/*
 * cMenu99.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU99_HPP_
#define INC_MENU_CMENU99_HPP_

#include "cMenu.hpp"
#include "cMenu992.hpp"
#include "cMenu994.hpp"
#include "cMenu998.hpp"



class cMenu99 : public cMenu{
public:
	cMenu99(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu99();
	cMenu * mGoToChild();
private:

	cMenu * Function1();
	cMenu * Function3();
	cMenu * Function5();
	cMenu * Function6();
	cMenu * Function7();
};

#endif /* INC_MENU_CMENU99_HPP_ */
