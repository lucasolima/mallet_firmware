/*
 * cMenu4.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU4_HPP_
#define INC_MENU_CMENU4_HPP_

#include "cMenu.hpp"

class cMenu4 : public cMenu {
public:
	cMenu4(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu4();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
};

#endif /* INC_MENU_CMENU4_HPP_ */
