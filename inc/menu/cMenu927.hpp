/*
 * cMenu927.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU927_HPP_
#define INC_MENU_CMENU927_HPP_

#include "cMenu.hpp"

class cMenu927 : public cMenu{
public:
	cMenu927(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu927();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();

};

#endif /* INC_MENU_CMENU927_HPP_ */
