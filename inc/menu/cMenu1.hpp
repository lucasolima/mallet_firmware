/*
 * cMenu1.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU1_HPP_
#define INC_MENU_CMENU1_HPP_

#include "cMenu.hpp"
#include "cMenuText.hpp"

class cMenu1 : public cMenu {
public:
	cMenu1(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu1();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
	cMenu * Function4();
	cMenu * Function5();

	Json::Value MsgInbox;
	Json::Value MsgOutbox;
	Json::Value MsgBuffer;

	string *text;

	void UpdateOutboxFile();
	void UpdateInboxFile();

	cMenu* ret_ptr ;

};

#endif /* INC_MENU_CMENU1_HPP_ */
