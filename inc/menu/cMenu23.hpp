/*
 * cMenu23.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU23_HPP_
#define INC_MENU_CMENU23_HPP_
#include "cMenu.hpp"

class cMenu23 : public cMenu{
public:
	cMenu23(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu23();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
};

#endif /* INC_MENU_CMENU23_HPP_ */
