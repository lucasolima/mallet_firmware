/*
 * cMenu96.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU96_HPP_
#define INC_MENU_CMENU96_HPP_

#include "cMenu.hpp"

class cMenu96 : public cMenu {
public:
	cMenu96(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu96();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
};

#endif /* INC_MENU_CMENU96_HPP_ */
