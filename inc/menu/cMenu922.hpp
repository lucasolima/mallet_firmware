/*
 * cMenu922.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU922_HPP_
#define INC_MENU_CMENU922_HPP_

#include "cMenu.hpp"

class cMenu922 : public cMenu {
public:
	cMenu922(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu922();
	cMenu * mGoToChild();

};

#endif /* INC_MENU_CMENU922_HPP_ */
