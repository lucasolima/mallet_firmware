/*
 * cMenu8.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU8_HPP_
#define INC_MENU_CMENU8_HPP_

#include "cMenu.hpp"

class cMenu8 : public cMenu {
public:
	cMenu8(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu8();
	cMenu * mGoToChild();
private:
	cMenu *Function1();
	cMenu *Function2();
	cMenu *Function3();
};

#endif /* INC_MENU_CMENU8_HPP_ */
