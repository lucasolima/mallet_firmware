/*
 * cMenu95.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU95_HPP_
#define INC_MENU_CMENU95_HPP_

#include "cMenu.hpp"

class cMenu95 : public cMenu {
public:
	cMenu95(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu95();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
	cMenu * Function4();
};

#endif /* INC_MENU_CMENU95_HPP_ */
