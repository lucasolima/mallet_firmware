/*
 * cMenu7.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU7_HPP_
#define INC_MENU_CMENU7_HPP_

#include "cMenu.hpp"
#include "cMenu71.hpp"
#include "cMenu72.hpp"

class cMenu7 : public cMenu {
public:
	cMenu7(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu7();
	cMenu * mGoToChild();
};

#endif /* INC_MENU_CMENU7_HPP_ */
