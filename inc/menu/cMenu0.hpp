/*
 * cMenu0.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU0_HPP_
#define INC_MENU_CMENU0_HPP_

#include "cMenu.hpp"

#include "cMenu0.hpp"
#include "cMenu1.hpp"
#include "cMenu2.hpp"
#include "cMenu4.hpp"
#include "cMenu6.hpp"
#include "cMenu7.hpp"
#include "cMenu8.hpp"
#include "cMenu9.hpp"
#include "cMenu10.hpp"
#include "cMenu11.hpp"

class cMenu0 : public cMenu{
public:
	cMenu0(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu0();
	cMenu * mGoToChild();
private:
	cMenu * Function3();
	cMenu * Function5();

};

#endif /* INC_MENU_CMENU0_HPP_ */
