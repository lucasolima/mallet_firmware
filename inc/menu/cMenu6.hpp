/*
 * cMenu6.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU6_HPP_
#define INC_MENU_CMENU6_HPP_

#include "cMenu.hpp"

class cMenu6 : public cMenu {
public:
	cMenu6(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu6();
	cMenu * mGoToChild();
private:
	cMenu* Function1();
	cMenu* Function2();
	cMenu* Function3();
};

#endif /* INC_MENU_CMENU6_HPP_ */
