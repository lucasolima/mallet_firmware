/*
 * cMenu91.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU91_HPP_
#define INC_MENU_CMENU91_HPP_

#include "cMenu.hpp"
#include "cMenu911.hpp"
#include "cMenu912.hpp"
#include "cMenu913.hpp"


class cMenu91 : public cMenu{
public:
	cMenu91(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu91();
	cMenu * mGoToChild();
private:
	cMenu * Function4();
	cMenu * Function5();
	cMenu * Function6();
	cMenu * Function7();


};

#endif /* INC_MENU_CMENU91_HPP_ */
