/*
 * cMenu10.hpp
 *
 *  Created on: May 31, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU10_HPP_
#define INC_MENU_CMENU10_HPP_

#include "cMenu.hpp"
#include "../bluetooth.hpp"
#include "cMenu104.hpp"

class cMenu10 : public cMenu {
public:
	cMenu10(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu10();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
	cMenu * Function4();
	cMenu * Function5();
	cMenu * Function6();

	char buff[20];
	char buf_ip[16];

};
#endif /* INC_MENU_CMENU10_HPP_ */
