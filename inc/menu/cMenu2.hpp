/*
 * cMenu2.h
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU2_HPP_
#define INC_MENU_CMENU2_HPP_

#include "cMenu.hpp"
#include "cMenu22.hpp"
#include "cMenu23.hpp"

class cMenu2 : public cMenu {
public:
	cMenu2(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu2();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function3();
	cMenu * Function4();
};

#endif /* INC_MENU_CMENU2_HPP_ */
