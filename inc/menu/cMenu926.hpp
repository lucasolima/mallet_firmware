/*
 * cMenu926.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU926_HPP_
#define INC_MENU_CMENU926_HPP_

#include "cMenu.hpp"

class cMenu926 : public cMenu{
public:
	cMenu926(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu926();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();

};

#endif /* INC_MENU_CMENU926_HPP_ */
