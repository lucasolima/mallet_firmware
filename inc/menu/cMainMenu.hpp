/*
 * cMainMenu.hpp
 *
 *  Created on: May 18, 2016
 *      Author: lucasolim
 */

#ifndef CMAINMENU_HPP_
#define CMAINMENU_HPP_

#include "cMenu.hpp"
#include "cMenu0.hpp"
#include "ctime"


class cMainMenu: public cMenu {

public:
	cMainMenu(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMainMenu();
	cMenu * mGoToChild();
	virtual int mScrollDown(int n);
	virtual int mScrollUp(int n);
	virtual int mJumpToLine(unsigned int n);
	virtual void mPrintMenu();
	int mSetRssi(int n);
	char mGetRssi();
	int mSetBattery(int n);
	char mGetBattery();
	void mSetBluetooth();
	void mResetBluetooth();
	void mSetMessage();
	void mResetMessage();
	void mSetRadioTime();
	virtual void mUpdate();
	virtual void mUpdateTime();
private:
	bool bluetooth;
	bool message;
	int battery;
	int rssi;
	float tx;
	float rx;
	bool ptt1;
	bool ptt2;
	int pot;
	int ch;
	bool TDM;
	char radio_time[6];
	char TDM_count[10];

};

#endif /* CMAINMENU_HPP_ */
