/*
 * cMenu912.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU912_HPP_
#define INC_MENU_CMENU912_HPP_

#include "cMenu.hpp"

class cMenu912 : public cMenu {
public:
	cMenu912(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu912();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();

};

#endif /* INC_MENU_CMENU912_HPP_ */
