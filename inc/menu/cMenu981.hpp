/*
 * cMenu981.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU981_HPP_
#define INC_MENU_CMENU981_HPP_

#include "cMenu.hpp"

class cMenu981: public cMenu {
public:
	cMenu981(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu981();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
	cMenu * Function3();
	cMenu * Function4();
	cMenu * Function5();
};

#endif /* INC_MENU_CMENU981_HPP_ */
