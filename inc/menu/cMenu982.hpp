/*
 * cMenu982.hpp
 *
 *  Created on: May 16, 2016
 *      Author: lucasolim
 */

#ifndef INC_MENU_CMENU982_HPP_
#define INC_MENU_CMENU982_HPP_

#include "cMenu.hpp"

class cMenu982: public cMenu {
public:
	cMenu982(cConfigurations& oConfigurations, cMenu* prnt);
	virtual ~cMenu982();
	cMenu * mGoToChild();
private:
	cMenu * Function1();
	cMenu * Function2();
};

#endif /* INC_MENU_CMENU982_HPP_ */
